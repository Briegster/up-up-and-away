package GameWorld;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import Config.Config;
import Entities.Attack;
import Entities.MovingEnemy;
import Entities.Player;
import Entities.Spike;
import Items.Coin;
import Items.HealthItemClass;
import Items.JumpItemClass;
import Items.Key;
import Items.ShieldItemClass;

public class GameWorldFrame extends JPanel implements ActionListener {
	/**
	 * 
	 */

	public Timer mainTimer;

	public LevelGrid currentLevel;
	public Player currentPlayer;

	Rectangle charac1Rect = new Rectangle(10, 440, 1, 1);
	Rectangle charac2Rect = new Rectangle(58, 440, 1, 1);
	Rectangle charac3Rect = new Rectangle(106, 440, 1, 1);
	Rectangle charac4Rect = new Rectangle(154, 440, 1, 1);
	Rectangle[] arrCharacRect = { charac1Rect, charac2Rect, charac3Rect, charac4Rect };

	int pixels = 16;
	public int camerax = 15 * 16;
	public int cameray = 15 * 16;
	public static boolean started = false;
	public static boolean died = false;
	public static int cheat3 = 0;
	public static boolean loaded = false;
	boolean masterStart = false;

	public static int score;
	Boolean unpassableTile;
	String tileType;
	public static JLabel lblHealth = new JLabel("");
	public static JLabel lblScore = new JLabel("");
	public static JLabel lblKeyCount = new JLabel("");

	private static final long serialVersionUID = 1L;

	public ArrayList<MovingEnemy> arrMovingSpike = new ArrayList<MovingEnemy>();
	public ArrayList<Spike> arrSpike = new ArrayList<Spike>();
	public ArrayList<Attack> arrAttack = new ArrayList<Attack>();
	public ArrayList<HealthItemClass> arrHealthItem = new ArrayList<HealthItemClass>();
	public ArrayList<JumpItemClass> arrJumpBoostItem = new ArrayList<JumpItemClass>();
	public ArrayList<ShieldItemClass> arrShieldItem = new ArrayList<ShieldItemClass>();
	public ArrayList<Gate> arrGate = new ArrayList<Gate>();
	public ArrayList<Key> arrKey = new ArrayList<Key>();
	public ArrayList<Coin> arrCoin = new ArrayList<Coin>();
	public ArrayList<Player> arrCharacters = new ArrayList<Player>();
	ImageIcon brickAni = new ImageIcon(Config.pathForStatic("Images/brickAnigif"));
	ImageIcon louAni = new ImageIcon(Config.pathForStatic("Images/playerAnigif"));
	ImageIcon steveAni = new ImageIcon(Config.pathForStatic("Images/steveAnigif"));
	ImageIcon andyAni = new ImageIcon(Config.pathForStatic("Images/andyAnigif"));

	public GameWorldFrame() {

		setLayout(null);

		setFocusable(true);
		// creating the timer
		mainTimer = new Timer(30, this);

	}

	public void init() throws FileNotFoundException {
		// ensuring that it does not perform this action twice.
		if (started == false) {
			// creating the level
			currentLevel = new LevelGrid(100, 100, LevelGrid.levelNum);
			// setting the character to the first one and then switching to
			// avoid a bug. then getting the controls
			currentPlayer = arrCharacters.get(0);

			currentPlayer.switchCharacter();
			Frames.OptionsFrame.getControls();
			// ensuring that it only adds one key listener to avoid a bug
			if (masterStart == false) {
				addKeyListener(currentPlayer);
				masterStart = true;
			}
			// Initializing some variables.
			Player.tempJumpHeight = currentPlayer.maxJump;
			started = true;

			Player.keya = false;
			Player.keys = false;
			Player.keyd = false;
			Player.keyw = false;

			lblHealth.setText(Integer.toString(currentPlayer.health));
			lblHealth.setBounds(0, 0, 48, 32);
			lblHealth.setForeground(Color.WHITE);
			lblHealth.setText(Integer.toString(currentPlayer.health));
			lblHealth.setIcon(new ImageIcon(Config.pathForStatic("Images/HeartIcon.gif")));
			add(lblHealth);

			lblScore.setBounds(0, 48, 48, 96);
			lblScore.setText(Integer.toString(score));
			lblScore.setForeground(Color.WHITE);
			lblScore.setIcon(new ImageIcon(Config.pathForStatic("Images/scoreIcon.gif")));
			add(lblScore);

			lblKeyCount.setBounds(0, 48, 48, 160);
			lblKeyCount.setText(Integer.toString(Player.keyCount));
			lblKeyCount.setForeground(Color.WHITE);
			lblKeyCount.setIcon(new ImageIcon(Config.pathForStatic("Images/keyIcon.gif")));
			add(lblKeyCount);

		}

		mainTimer.start();

	}

	@SuppressWarnings("static-access")
	@Override
	public void paint(Graphics g) {

		changeLevel();
		// painting all of the tiles based on the string value of their
		// corresponding grid coordinates.
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		for (int x = 0; x < currentLevel.width; x++) {
			for (int y = 0; y < currentLevel.height; y++) {
				g2d.drawImage(currentLevel.currentBackTile.getImage(), x * pixels, y * pixels, null);

			}

		}

		for (int x = 0; x < currentLevel.width; x++) {
			for (int y = 0; y < currentLevel.height; y++) {

				if (currentLevel.tiles[x][y].equals("platform")) {
					g2d.drawImage(currentLevel.currentPlatTile.getImage(), x * pixels - currentPlayer.cornerx + camerax, y * pixels - currentPlayer.cornery
							+ cameray, null);

				} else if (currentLevel.tiles[x][y].equals("LouDoor")) {
					g2d.drawImage(currentLevel.louDoorTile.getImage(), x * pixels - currentPlayer.cornerx + camerax, y * pixels - currentPlayer.cornery
							+ cameray, null);
				} else if (currentLevel.tiles[x][y].equals("SteveDoor")) {
					g2d.drawImage(currentLevel.steveDoorTile.getImage(), x * pixels - currentPlayer.cornerx + camerax, y * pixels - currentPlayer.cornery
							+ cameray, null);
				} else if (currentLevel.tiles[x][y].equals("BrickDoor")) {
					g2d.drawImage(currentLevel.brickDoorTile.getImage(), x * pixels - currentPlayer.cornerx + camerax, y * pixels - currentPlayer.cornery
							+ cameray, null);
				} else if (currentLevel.tiles[x][y].equals("AndyDoor")) {
					g2d.drawImage(currentLevel.andyDoorTile.getImage(), x * pixels - currentPlayer.cornerx + camerax, y * pixels - currentPlayer.cornery
							+ cameray, null);

				} else if (currentLevel.tiles[x][y].equals("cloud")) {
					g2d.drawImage(currentLevel.cloudTile.getImage(), x * pixels - currentPlayer.cornerx + camerax + currentPlayer.cloudmovespeed, y * pixels
							- currentPlayer.cornery + cameray, null);

				} else if (currentLevel.tiles[x][y].equals("water")) {
					g2d.drawImage(currentLevel.waterTile.getImage(), x * pixels - currentPlayer.cornerx + camerax,
							y * pixels - currentPlayer.cornery + cameray, null);
				}
			}

		}

		// painting all of the items and entities
		for (int i = 0; i < arrCharacters.size(); i++) {

			arrCharacters.get(i).draw(g2d, -currentPlayer.cornerx + cameray, -currentPlayer.cornery + camerax);

		}

		for (int i = 0; i < arrMovingSpike.size(); i++) {

			arrMovingSpike.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);

		}
		for (int i = 0; i < arrSpike.size(); i++) {

			arrSpike.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);
		}
		for (int i = 0; i < arrAttack.size(); i++) {

			arrAttack.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);
		}
		for (int i = 0; i < arrHealthItem.size(); i++) {

			arrHealthItem.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);

		}
		for (int i = 0; i < arrJumpBoostItem.size(); i++) {

			arrJumpBoostItem.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);
		}
		for (int i = 0; i < arrShieldItem.size(); i++) {

			arrShieldItem.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);
		}
		for (int i = 0; i < arrKey.size(); i++) {

			arrKey.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);

		}

		for (int i = 0; i < arrGate.size(); i++) {

			arrGate.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);
		}
		for (int i = 0; i < arrCoin.size(); i++) {

			arrCoin.get(i).draw(g2d, -currentPlayer.cornerx + camerax, -currentPlayer.cornery + cameray);
		}
		// the special effects
		if (ShieldItemClass.shieldBoolean) {
			g.setColor(Color.RED);
			g.drawRect(camerax, cameray, currentPlayer.width * 16 - 1, currentPlayer.height * 16 - 1);
		}
		if (JumpItemClass.boostBoolean) {
			g.setColor(Color.YELLOW);
			g.drawLine(camerax, cameray + currentPlayer.height * 16 - 1, camerax + currentPlayer.width * 16 - 1, cameray + currentPlayer.height * 16 - 1);
		}

		g.setColor(Color.YELLOW);
		g.fillRect(camerax, cameray, 8, 8);
		// the current character indicator.
		for (int c = 0; c < arrCharacters.size(); c++) {

			if (c != Player.currentPlayerToken) {
				g.setColor(Color.RED);
				g.fillRect((int) arrCharacRect[c].getX(), (int) arrCharacRect[c].getY(), arrCharacters.get(c).width * 8, arrCharacters.get(c).height * 8);
			} else {
				g.setColor(Color.YELLOW);
				g.fillRect((int) arrCharacRect[c].getX(), (int) arrCharacRect[c].getY(), arrCharacters.get(c).width * 8, arrCharacters.get(c).height * 8);
			}
		}
		// painting all of the labels and the help message
		g.setColor(Color.WHITE);
		g.drawString(LevelGrid.helpMessage, camerax, cameray - 5);

		lblHealth.paint(g);
		lblScore.paint(g);
		lblKeyCount.paint(g);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// painting all of the moving objects
		for (int i = 0; i < arrMovingSpike.size(); i++) {
			arrMovingSpike.get(i).move();
		}
		for (int i = 0; i < arrAttack.size(); i++) {

			arrAttack.get(i).move();
		}
		for (int p = 0; p < arrCharacters.size(); p++) {
			arrCharacters.get(p).update();
		}

		repaint();

	}

	@SuppressWarnings("static-access")
	public void changeLevel() {
		// checking if all characters are at the door.
		if (currentLevel.tiles[currentPlayer.pixelsToX() + (currentPlayer.width / 2)][currentPlayer.pixelsToY() + (currentPlayer.height / 2)]
				.equals(currentPlayer.name + "Door")) {

			int allAtDoor = 0;
			currentPlayer.atDoor = true;

			for (int p = 0; p < arrCharacters.size(); p++) {
				if (arrCharacters.get(p).atDoor == true) {
					allAtDoor++;

				}

			}
			// if they are all at the door, and the user isn't testing the level
			// change the level.
			if (allAtDoor == arrCharacters.size() && Frames.EditorFrame.testingLevel == false) {
				currentLevel.erase();
				currentLevel.levelNum++;
				currentLevel.changeLevel(currentLevel.levelNum);
				if (LevelGrid.levelNum < LevelGrid.arrLevels.size() - 1) {
					Player.currentPlayerToken = 0;
					GameWorld.gameWorldFrame.currentPlayer.switchCharacter();
					GameWorld.gameWorldFrame.currentPlayer.grounded = true;
				}

			}

		} else {
			currentPlayer.atDoor = false;
		}
		// this handles the cheats to get to a certain level quickly.
		if (Player.cheat1 == 5 && Player.cheat2 == 5) {
			if (Player.keya == true && Player.keys == true && Player.keySpace == true) {

				currentLevel.levelNum = cheat3;
				LevelGrid.erase();
				Player.cheat1 = 0;
				Player.cheat2 = 0;
				cheat3 = 0;
				currentLevel.changeLevel(currentLevel.levelNum);

				Player.currentPlayerToken = 0;
				currentPlayer.switchCharacter();

			}

		}
	}

}
