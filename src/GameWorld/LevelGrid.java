package GameWorld;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import javax.swing.ImageIcon;

import Audio.BackgroundMusic;
import Config.Config;
import Entities.MovingEnemy;
import Entities.Player;
import Entities.Spike;
import Frames.EditorFrame;
import Frames.OptionsFrame;
import Items.Coin;
import Items.HealthItemClass;
import Items.JumpItemClass;
import Items.Key;
import Items.ShieldItemClass;

public class LevelGrid {

	private static final ImageIcon spikeIcon = null;
	public static int width;
	public static int height;

	public static String[][] tiles;
	public static int levelNum = 1;

	int chapter = 0;

	static ImageIcon platformTile = new ImageIcon(Config.pathForStatic("Images/dirttile.gif"));
	static ImageIcon backDirtTile = new ImageIcon(Config.pathForStatic("Images/backdirttile.jpg"));

	static ImageIcon stoneTile = new ImageIcon(Config.pathForStatic("Images/stone.gif"));
	static ImageIcon stonePlatTile = new ImageIcon(Config.pathForStatic("Images/stonePlatform.gif"));
	static ImageIcon skyTile = new ImageIcon(Config.pathForStatic("Images/skytile.jpg"));
	ImageIcon cloudTile = new ImageIcon(Config.pathForStatic("Images/cloud.gif"));

	static ImageIcon currentBackTile = backDirtTile;
	static ImageIcon currentPlatTile = platformTile;

	ImageIcon waterTile = new ImageIcon(Config.pathForStatic("Images/waterAni.gif"));

	ImageIcon spikeTile = new ImageIcon(Config.pathForStatic("Images/Spike.gif"));
	ImageIcon movingSpikeIcon = new ImageIcon(Config.pathForStatic("Images/movingSpike.gif"));
	ImageIcon rock = new ImageIcon(Config.pathForStatic("Images/Rock.gif"));

	ImageIcon louDoorTile = new ImageIcon(Config.pathForStatic("Images/LouDoorGrey.gif"));
	ImageIcon steveDoorTile = new ImageIcon(Config.pathForStatic("Images/SteveDoorGrey.gif"));
	ImageIcon brickDoorTile = new ImageIcon(Config.pathForStatic("Images/BrickDoorGrey.gif"));
	ImageIcon andyDoorTile = new ImageIcon(Config.pathForStatic("Images/AndyDoorGrey.gif"));

	ImageIcon gateTile = new ImageIcon(Config.pathForStatic("Images/gateClosed.gif"));
	ImageIcon LouAni = new ImageIcon(Config.pathForStatic("Images/playerAni.gif"));
	ImageIcon brickAni = new ImageIcon(Config.pathForStatic("Images/brickAni.gif"));

	static ArrayList<Integer> arrLevels = new ArrayList<Integer>();
	ArrayList<String> arrControls = new ArrayList<String>();
	public static ArrayList<String> arrHelpMessages = new ArrayList<String>();

	Map<Integer, String> levelMap = new TreeMap<Integer, String>();

	static int levelChapter;

	public static String helpMessage;

	@SuppressWarnings("static-access")
	public LevelGrid(int width, int height, int levelNum) {
		this.width = width;
		this.height = height;
		// adds a list of key words to look for in the help message.
		arrControls.add("JUMP");
		arrControls.add("LEFT");
		arrControls.add("DOWN");
		arrControls.add("RIGHT");
		arrControls.add("OPEN");
		arrControls.add("SHOOT");
		arrControls.add("SWITCH");
		// creates the grid
		this.tiles = new String[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				tiles[x][y] = "";

			}
		}
		changeLevel(GameWorld.gameWorldFrame.currentLevel.levelNum);

	}

	public static void erase() {
		// wipes the grid
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				tiles[x][y] = ("");

			}
		}
		// setting all booleans to false, and clearing all of the arraylists.
		ShieldItemClass.shieldBoolean = false;
		JumpItemClass.boostBoolean = false;
		Player.keyCount = 0;
		GameWorldFrame.lblKeyCount.setText(Integer.toString(Player.keyCount));
		GameWorld.gameWorldFrame.arrMovingSpike.clear();
		GameWorld.gameWorldFrame.arrSpike.clear();
		GameWorld.gameWorldFrame.arrAttack.clear();

		GameWorld.gameWorldFrame.arrHealthItem.clear();
		GameWorld.gameWorldFrame.arrJumpBoostItem.clear();
		GameWorld.gameWorldFrame.arrShieldItem.clear();
		GameWorld.gameWorldFrame.arrCoin.clear();
		GameWorld.gameWorldFrame.arrKey.clear();
		GameWorld.gameWorldFrame.arrGate.clear();
		GameWorld.gameWorldFrame.arrCharacters.clear();

	}

	public static void changeChapter() {
		// changing the chapter

		if (levelChapter == 0) {
			BackgroundMusic.changeMusic("bgm1");
			currentBackTile = backDirtTile;
			currentPlatTile = platformTile;

		} else if (levelChapter == 1) {
			BackgroundMusic.changeMusic("bgm2");
			currentBackTile = stoneTile;
			currentPlatTile = stonePlatTile;

		} else if (levelChapter == 2) {
			BackgroundMusic.changeMusic("bgm1");
			currentBackTile = skyTile;
			currentPlatTile = platformTile;

		}
	}

	public void changeLevel(int nextLevel) {
		// reading in the information of the levels.
		File levelNumberFile = new File(Config.pathForEditable("Levels/levelNumber.txt"));

		Scanner scanner;
		int countTo = 0;
		try {
			scanner = new Scanner(levelNumberFile);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] splits = line.split(" ");
				if (splits.length != 3) {

					continue;

				}

				// ensuring that it doesn't read in the levels info more than
				// once.
				if (GameWorld.gameWorldFrame.masterStart == false) {
					arrLevels.add(Integer.parseInt(splits[0]));
					arrHelpMessages.add(splits[1]);
				}
				// finding the chapter of the level
				countTo++;
				if (levelNum == 777 && line.contains("777")) {

					levelChapter = Integer.parseInt(splits[2]);

				} else if (countTo == levelNum) {
					levelChapter = Integer.parseInt(splits[2]);
				}

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// matching up the help message with its level
		for (int v = 0; v < arrLevels.size(); v++) {

			levelMap.put(arrLevels.get(v), arrHelpMessages.get(v));

		}
		// setting the helpMessage
		if (levelNum < arrLevels.size() || EditorFrame.testingLevel == true) {
			if (EditorFrame.testingLevel == false) {
				helpMessage = levelMap.get(levelNum).toString();

			}
			if (helpMessage.equals("null")) {
				helpMessage = "";
			}
			helpMessage = helpMessage.replaceAll("_", " ");
			// swapping the underscores back to spaces for readability.
			OptionsFrame.getControls();
			for (int i = 0; i < arrControls.size(); i++) {
				// checking if the message contains one of the key words from
				// the controls array
				// if so it swaps it out for the character in the options
				// screen, dynamically changing the message prompt depending of
				// your controls.

				if (helpMessage.contains(arrControls.get(i))) {
					String tmpControl = OptionsFrame.arrCharControls.get(i).toString();
					if (OptionsFrame.arrCharControls.get(i).toString().equals(" ")) {

						tmpControl = "space";
					}
					helpMessage = helpMessage.replaceAll(arrControls.get(i), tmpControl);

				}
			}

			levelLoader(Config.pathForEditable("Levels/Level" + levelNum + ".txt"));

			changeChapter();

		} else {
			// checking if its the last level, and if so, you win the game
			if (EditorFrame.testingLevel == false) {
				GameWorld.gameWorldFrame.mainTimer.stop();
				GameWorld.window.setVisible(false);
				WinFrame.frame.setVisible(true);
			}
		}

	}

	public void levelLoader(String filename) {
		// loading in the details for all the tiles.
		File f = new File(filename);

		Scanner scanner;
		try {

			scanner = new Scanner(f);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] splits = line.split(" ");

				if (splits.length != 3) {

					continue;

				}

				int tileX = Integer.parseInt(splits[0]);
				int tileY = Integer.parseInt(splits[1]);
				String tileType = splits[2];
				tiles[tileX][tileY] = tileType;
				// if the tiles name is a certain word, it adds to that objects
				// array list, bringing it into existence.
				if (tiles[tileX][tileY].equals("spike")) {
					GameWorld.gameWorldFrame.arrSpike.add(new Spike(tileX, tileY, spikeIcon, 0));
				}
				if (tiles[tileX][tileY].equals("movingSpike")) {
					GameWorld.gameWorldFrame.arrMovingSpike
							.add(new MovingEnemy(tileX, tileY, GameWorld.gameWorldFrame.currentLevel, movingSpikeIcon, 0, false));
				}
				if (tiles[tileX][tileY].equals("healthItem")) {
					GameWorld.gameWorldFrame.arrHealthItem.add(new HealthItemClass(tileX, tileY));

				}
				if (tiles[tileX][tileY].equals("jumpBoostItem")) {
					GameWorld.gameWorldFrame.arrJumpBoostItem.add(new JumpItemClass(tileX, tileY));
				}
				if (tiles[tileX][tileY].equals("jumpBoostItem")) {
					GameWorld.gameWorldFrame.arrJumpBoostItem.add(new JumpItemClass(tileX, tileY));
				}
				if (tiles[tileX][tileY].equals("shieldItem")) {
					GameWorld.gameWorldFrame.arrShieldItem.add(new ShieldItemClass(tileX, tileY));
				}
				if (tiles[tileX][tileY].equals("gate")) {
					GameWorld.gameWorldFrame.arrGate.add(new Gate(tileX, tileY, false, gateTile, false));

				}
				if (tiles[tileX][tileY].equals("key")) {
					GameWorld.gameWorldFrame.arrKey.add(new Key(tileX, tileY));
				}
				if (tiles[tileX][tileY].equals("coin")) {
					GameWorld.gameWorldFrame.arrCoin.add(new Coin(tileX, tileY));
				}
				if (tiles[tileX][tileY].equals("louSpawn")) {

					GameWorld.gameWorldFrame.arrCharacters.add(new Player("Lou", LouAni, GameWorld.gameWorldFrame.currentLevel,
							GameWorld.gameWorldFrame.arrMovingSpike, GameWorld.gameWorldFrame.arrSpike, tileX * 16, tileY * 16, 8, 2, 2, 14, 100, "", false,
							false, false, 0));

				}
				if (tiles[tileX][tileY].equals("brickSpawn")) {
					GameWorld.gameWorldFrame.arrCharacters.add(new Player("Brick", brickAni, GameWorld.gameWorldFrame.currentLevel,
							GameWorld.gameWorldFrame.arrMovingSpike, GameWorld.gameWorldFrame.arrSpike, tileX * 16, tileY * 16, 4, 3, 3, 10, 150, "", false,
							false, false, 0));
				}
				if (tiles[tileX][tileY].equals("steveSpawn")) {
					GameWorld.gameWorldFrame.arrCharacters.add(new Player("Steve", GameWorld.gameWorldFrame.steveAni, GameWorld.gameWorldFrame.currentLevel,
							GameWorld.gameWorldFrame.arrMovingSpike, GameWorld.gameWorldFrame.arrSpike, tileX * 16, tileY * 16, 8, 4, 1, 18, 100, "", false,
							false, false, 0));
				}
				if (tiles[tileX][tileY].equals("andySpawn")) {
					GameWorld.gameWorldFrame.arrCharacters.add(new Player("Andy", GameWorld.gameWorldFrame.andyAni, GameWorld.gameWorldFrame.currentLevel,
							GameWorld.gameWorldFrame.arrMovingSpike, GameWorld.gameWorldFrame.arrSpike, tileX * 16, tileY * 16, 4, 2, 4, 10, 100, "", false,
							false, false, 0));
				}

			}

		}

		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
