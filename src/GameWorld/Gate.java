package GameWorld;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import Audio.SFX;
import Config.Config;
import Entities.Player;

public class Gate {
	public static int height = 2;
	static int width = 1;
	public int cornerx;
	public int cornery;
	public boolean touching = true;
	public boolean opened;

	ImageIcon gateTile = new ImageIcon(Config.pathForStatic("Images/gateClosed.gif"));
	static ImageIcon gateClosedTile = new ImageIcon(Config.pathForStatic("Images/gateOpened.gif"));

	public Gate(int x, int y, boolean opened, ImageIcon gateTile, boolean touching) {
		this.cornerx = x * 16;
		this.cornery = y * 16;
		this.opened = opened;
		this.touching = touching;

	}

	public static void gateInteract() {
		// changing the tile of the gate if its open, and the user is attempting
		// to open it.
		for (int i = 0; i < GameWorld.gameWorldFrame.arrGate.size(); i++) {

			if (GameWorld.gameWorldFrame.arrGate.get(i).touching == true && Player.keye == true && Player.keyCount >= 1) {

				Player.keyCount -= 1;
				GameWorldFrame.lblKeyCount.setText(Integer.toString(Player.keyCount));
				SFX.itemSound("gateOpen");
				GameWorld.gameWorldFrame.arrGate.get(i).opened = true;

				GameWorld.gameWorldFrame.arrGate.get(i).gateTile = gateClosedTile;

			}

		}

	}

	public Rectangle getGateBounds() {
		return new Rectangle(cornerx, cornery, 16, 32);

	}

	public void draw(Graphics2D p, int startx, int starty) {
		// TODO Auto-generated method stub

		p.drawImage(gateTile.getImage(), cornerx + startx, cornery + starty, null);

	}

}
