package GameWorld;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Config.Config;

public class WinFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static WinFrame frame = new WinFrame();

	public void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					// initialising the win frame.
					frame.setVisible(false);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public WinFrame() {
		setBackground(Color.BLACK);
		setTitle("Congratulations!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblThisIsWhere = new JLabel("");
		// setting the icon to the gif of the ending movie.

		lblThisIsWhere.setIcon(new ImageIcon(Config.pathForStatic("Images/WinMovie.gif")));
		lblThisIsWhere.setVisible(true);
		lblThisIsWhere.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				// returning to the menu
				dispose();
				GameWorld.menuFrame.btnResume.setVisible(false);
				GameWorld.menuFrame.lblProfileName.setText("Profile: " + GameWorld.menuFrame.profileName + " | Level: " + LevelGrid.levelNum + " | Score: "
						+ GameWorldFrame.score);
				GameWorld.menuFrame.setVisible(true);

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		contentPane.add(lblThisIsWhere, BorderLayout.CENTER);

	}

}
