package GameWorld;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Audio.BackgroundMusic;
import Config.Config;
import Frames.Editor;
import Frames.HighScoresFrame;
import Frames.HighScoresInit;
import Frames.Options;
import Frames.OptionsFrame;

public class MenuFrame extends JFrame implements MouseListener, ListSelectionListener, ActionListener {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	public JPanel mainMenuPane;
	private JList ProfileList;
	private JScrollPane listScrollPane;

	JButton btnDelete = new JButton("Delete");
	JButton btnSave = new JButton("Save");
	public JButton btnResume = new JButton("Resume");
	JButton btnNewGame = new JButton("New Game");
	private JTextField nameField;

	final JPanel ProfilePanel = new JPanel();
	public JLabel lblProfileName = new JLabel("Profile: Guest | Level: 0 | Score: 0\n");
	Vector<String> VecProfiles = new Vector<String>();
	boolean loadBtnPressed = false;
	boolean loadedNames = false;
	boolean outofProfilePanel = false;
	public String profileName = "Guest";
	int deleteConfirm;
	int scoreOnLoad;

	private static final String profileNamesPath = Config.pathForEditable("Profiles/ProfileNames.txt");

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {

				try {
					MenuFrame frame = new MenuFrame();
					frame.setVisible(true);
					frame.setResizable(false);
					// Initializing the Options menu so that the controls can be
					// gathered.
					Options.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					Options.window.getContentPane().removeAll();
					Options.window.getContentPane().add(Options.OptionsFrame);
					Options.window.validate();
					Options.window.setSize(500, 520);
					Options.window.setResizable(false);

					OptionsFrame.getControls();

					BackgroundMusic.changeMusic("bgm1");

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the frame.
	 */

	public MenuFrame() {
		// Setting up the frame
		setPreferredSize(new Dimension(480, 500));
		setBackground(Color.LIGHT_GRAY);
		setForeground(Color.WHITE);

		setTitle("Up Up and Away");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds(100, 100, 480, 500);

		mainMenuPane = new JPanel();
		mainMenuPane.setBackground(Color.BLACK);
		mainMenuPane.setBorder(new EmptyBorder(1, 1, 1, 1));
		setContentPane(mainMenuPane);

		gatherProfiles();

		mainMenuPane.setLayout(null);
		btnNewGame.setToolTipText("Start a New Game");

		btnNewGame.setIcon(new ImageIcon(Config.pathForStatic("Images/newgame.gif")));
		btnNewGame.setBounds(177, 179, 100, 26);
		mainMenuPane.add(btnNewGame);
		btnNewGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Starting a new game
				LevelGrid.levelNum = 1;
				GameWorldFrame.score = 0;
				LevelGrid.erase();
				GameWorldFrame.started = false;

				playGame();
			}
		});
		btnResume.setToolTipText("Resume game");
		btnResume.setIcon(new ImageIcon(Config.pathForStatic("Images/PlayIcon.gif")));
		btnResume.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				playGame();
			}
		});
		btnResume.setBounds(177, 150, 100, 26);
		btnResume.setVisible(false);
		mainMenuPane.add(btnResume);

		JButton btnProfiles = new JButton("Profiles");
		btnProfiles.setToolTipText("Save, load or delete a profile");
		btnProfiles.setIcon(new ImageIcon(Config.pathForStatic("Images/LoadIcon.gif")));
		btnProfiles.setBounds(177, 208, 100, 26);
		mainMenuPane.add(btnProfiles);
		btnProfiles.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Showing or hiding the profiles pane, calling the
				// gatherProfiles method.
				if (loadBtnPressed == false) {
					gatherProfiles();
					ProfilePanel.setVisible(true);
					loadBtnPressed = true;

				} else {

					ProfilePanel.setVisible(false);
					loadBtnPressed = false;
				}

			}

		});

		JButton btnExit = new JButton("Exit");
		btnExit.setToolTipText("Exit the game");
		btnExit.setIcon(new ImageIcon(Config.pathForStatic("Images/ExitIcon.gif")));
		btnExit.setBounds(177, 448, 100, 24);
		mainMenuPane.add(btnExit);
		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		// Setting up the profiles panel with the list, and the scroll pane, and
		// the text field.
		ProfilePanel.setBounds(134, 235, 205, 150);

		mainMenuPane.add(ProfilePanel);
		ProfilePanel.setVisible(false);
		ProfileList = new JList(VecProfiles);
		ProfileList.setToolTipText("Available profiles");

		ProfileList.addListSelectionListener(this);

		ProfilePanel.setLayout(null);
		ProfileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		ProfileList.setLayoutOrientation(JList.VERTICAL);
		ProfileList.setVisibleRowCount(VecProfiles.size());

		listScrollPane = new JScrollPane(ProfileList);
		listScrollPane.setBounds(0, 0, 205, 84);
		ProfilePanel.add(listScrollPane);

		listScrollPane.setVisible(true);
		listScrollPane.setMinimumSize(new Dimension(100, 50));
		listScrollPane.setPreferredSize(getPreferredSize());
		listScrollPane.setViewportView(ProfileList);

		nameField = new JTextField();
		nameField.setToolTipText("Save profile name");
		nameField.setBounds(0, 83, 205, 29);

		ProfilePanel.add(nameField);

		final JButton btnSave_1 = new JButton("Save");
		btnSave_1.setToolTipText("Save a profile");
		btnSave_1.setIcon(new ImageIcon(Config.pathForStatic("Images/saveIcon.jpg")));
		btnSave_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					// Checking if the user wants to override a save or create a
					// new profile.
					String name = nameField.getText();
					if (VecProfiles.contains(name)) {
						int overRideSure = JOptionPane.showConfirmDialog(getParent(), "Are you sure you want to override save file " + nameField.getText());

						if (overRideSure == 0) {
							if (name.equals("") || name.contains(" ")) { // ||
								JOptionPane.showMessageDialog(getParent(), "You must enter a valid name that is not nothing and does not contain a space");

							} else {
								saveProfile();

							}
						}

					} else {

						if (name.equals("") || name.contains(" ")) {
							JOptionPane.showMessageDialog(getParent(), "You must enter a valid name that is not nothing and does not contain a space");

						} else {
							saveProfile();
							loadProfile();
						}

					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		btnSave_1.setBounds(0, 115, 62, 29);
		ProfilePanel.add(btnSave_1);

		final JButton btnDelete_1 = new JButton("Delete");
		btnDelete_1.setToolTipText("Delete a profile");
		btnDelete_1.setIcon(new ImageIcon(Config.pathForStatic("Images/ExitIcon.gif")));
		btnDelete_1.setVisible(true);
		btnDelete_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// checking if the user is sure they want to delete the profile.
				if (ProfileList.getSelectedValue() != null) {
					deleteConfirm = JOptionPane.showConfirmDialog(getParent(), "Are you sure you want to delete the profile " + ProfileList.getSelectedValue()
							+ "?");
					if (deleteConfirm == 0) {

						deleteProfile(true);
						JOptionPane.showMessageDialog(getParent(), "Profile deleted.");

					}

				}

			}
		});

		btnDelete_1.setBounds(125, 115, 74, 29);
		ProfilePanel.add(btnDelete_1);

		JButton btnLoad = new JButton("Load");
		btnLoad.setToolTipText("Load a profile");
		btnLoad.setIcon(new ImageIcon(Config.pathForStatic("Images/LoadIcon.gif")));
		btnLoad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// loading the profile
				ProfileList.clearSelection();

				loadProfile();

			}
		});
		btnLoad.setBounds(60, 115, 62, 29);
		ProfilePanel.add(btnLoad);

		JButton btnHighscores = new JButton("Highscores");
		btnHighscores.setToolTipText("See highscores of current profiles");
		btnHighscores.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Initiallising the high scores screen and showing it.
				setFocusable(false);
				setVisible(false);
				HighScoresFrame.getProfiles();
				HighScoresInit.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				HighScoresInit.window.getContentPane().removeAll();
				HighScoresInit.window.getContentPane().add(HighScoresInit.highscoresFrame);
				HighScoresInit.window.validate();
				HighScoresInit.window.setSize(480, 500);
				HighScoresInit.window.setResizable(false);
				HighScoresInit.window.setVisible(true);

			}
		});
		btnHighscores.setIcon(new ImageIcon(Config.pathForStatic("Images/scoreIcon.gif")));
		btnHighscores.setBounds(177, 242, 100, 26);
		mainMenuPane.add(btnHighscores);

		final JButton btnEditor = new JButton("Editor");
		btnEditor.setToolTipText("Edit existing, or create new levels");

		mainMenuPane.add(btnEditor);
		btnEditor.setBounds(177, 280, 100, 26);
		btnEditor.setIcon(new ImageIcon(Config.pathForStatic("Images/EditorIcon.gif")));
		btnEditor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Initializing there editor screen and showing it.
				setFocusable(false);
				setVisible(false);
				Editor.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				Editor.window.getContentPane().removeAll();
				Editor.window.getContentPane().add(Editor.editorFrame);
				Editor.window.validate();
				Editor.window.setResizable(false);
				Editor.editorFrame.repaint();
				Editor.window.setSize(1200, 800);
				Editor.editorFrame.initOrErase();
				Editor.window.setVisible(true);

			}
		});

		final JButton btnOptions = new JButton("Options");
		btnOptions.setToolTipText("Change controls or sound volume");

		btnOptions.setIcon(new ImageIcon(Config.pathForStatic("Images/OptionsIcon.gif")));
		btnOptions.setBounds(177, 316, 100, 26);
		mainMenuPane.add(btnOptions);
		btnOptions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Showing the Options screen since it's already been
				// initialized.
				setFocusable(false);
				setVisible(false);
				Options.window.setVisible(true);

			}
		});

		JPanel namePanel = new JPanel();
		namePanel.setBounds(1, 1, 478, 26);
		namePanel.setBackground(Color.BLACK);
		mainMenuPane.add(namePanel);
		lblProfileName.setToolTipText("Current profiles name, level, and score");
		namePanel.add(lblProfileName);
		lblProfileName.setForeground(Color.WHITE);
		lblProfileName.setBounds(6, 6, 61, 16);

		JLabel lblBackground = new JLabel("Background");
		lblBackground.setBounds(1, 27, 478, 450);
		mainMenuPane.add(lblBackground);

		lblBackground.setIcon(new ImageIcon(Config.pathForStatic("Images/GameBackground.jpg")));

		lblBackground.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {

				// Hiding the profiles panel if the user clicks out of it.
				if (outofProfilePanel) {

					btnOptions.setVisible(true);
					btnEditor.setVisible(true);
					ProfilePanel.setVisible(false);
					loadBtnPressed = false;
					ProfileList.clearSelection();
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				outofProfilePanel = true;
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		class namePanel extends JComponent {
			@Override
			protected void paintComponent(Graphics g) {
				lblProfileName.paint(g);
			}
		}

	}

	public void gatherProfiles() {
		// Loading all of the profiles once from the profiles name text file.
		// Adding them to the vector of profiles
		if (loadedNames == false) {

			loadedNames = true;
			File f = new File(profileNamesPath);
			Scanner scanner;

			try {
				try {
					scanner = new Scanner(f);
				} catch (FileNotFoundException e) {
					File newProfiles = new File(Config.pathForEditable(profileNamesPath));
					Config.makeEditableDirs();
					(new PrintWriter(f)).write("");
					return;
				}
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					String[] splits = line.split(" ");

					if (splits.length != 2) {

						continue;

					}
					VecProfiles.add(splits[0]);

				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("resource")
	public void saveProfile() throws IOException {
		// Creating a file with the name of the nameField
		String stringValue = nameField.getText();

		File f = new File(Config.pathForEditable("Profiles/Profile_" + stringValue + ".txt"));
		String f1 = null;
		// Checking if the file exists and is in the directory. If so it deletes
		// it.
		if (f.exists() && !f.isDirectory()) {
			f1 = f.toString();
			deleteProfile(false);

		} else {
			// In the profiles name file, it writes a new line adding in the new
			// profile.
			PrintWriter outputStream;
			try {

				outputStream = new PrintWriter(new BufferedWriter(new FileWriter(profileNamesPath, true)));
				outputStream.println(stringValue + " " + GameWorldFrame.score);

				outputStream.close(); // saves and closes file
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// adding the new profile to the vector and updating the list.
		f1 = (Config.pathForEditable("Profiles/Profile_" + stringValue + ".txt"));
		if (VecProfiles.contains(stringValue) == false && (stringValue != null || stringValue != "")) {

			VecProfiles.addElement(stringValue);
			ProfileList.setListData(VecProfiles);
			listScrollPane.revalidate();
			listScrollPane.repaint();

		}

		// Writing the proifles information into the file.
		if (stringValue != null || stringValue != "") {

			try {
				PrintWriter out = new PrintWriter(f1);

				out.print(LevelGrid.levelNum + " ");
				out.print(stringValue + " ");
				out.print(OptionsFrame.jumpChar + " ");
				out.print(OptionsFrame.leftChar + " ");
				out.print(OptionsFrame.downChar + " ");
				out.print(OptionsFrame.rightChar + " ");
				out.print(OptionsFrame.openChar + " ");
				if (OptionsFrame.attackChar == (" ").charAt(0)) {
					out.print("_" + " ");
				} else {
					out.print(OptionsFrame.attackChar + " ");
				}

				out.print(OptionsFrame.nextCharacChar + " ");
				out.print(OptionsFrame.Charac1Char + " ");
				out.print(OptionsFrame.Charac2Char + " ");
				out.print(OptionsFrame.Charac3Char + " ");
				out.print(OptionsFrame.Charac4Char + " ");
				out.print(GameWorldFrame.score + " ");
				out.print("ZZZ");

				// outputStream.flush(); // just saves it

				out.close(); // saves and closes file

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		ProfileList.setSelectedValue(stringValue, true);

		profileName = stringValue;

	}

	public void loadProfile() {
		// restarting the game, with the new profiles save data.
		GameWorld.gameWorldFrame.mainTimer.stop();
		LevelGrid.erase();
		profileName = nameField.getText();
		gatherSaveData();

		lblProfileName.setText("Profile: " + profileName + " | Level: " + LevelGrid.levelNum + " | Score: " + GameWorldFrame.score);
		btnResume.setVisible(true);

		GameWorldFrame.loaded = true;
		GameWorldFrame.started = false;

	}

	public void gatherSaveData() {
		// reading in the profiles text file and setting the saved variables
		// into the game.
		Scanner scanner;

		File f = new File(Config.pathForEditable("Profiles/Profile_" + profileName + ".txt"));

		try {

			scanner = new Scanner(f);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] splits = line.split(" ");

				for (int i = 0; i < splits.length; i++) {

					if (splits[i] != "ZZZ") {
						continue;
					}

				}

				LevelGrid.levelNum = Integer.parseInt(splits[0]);
				OptionsFrame.txtJump.setText(((splits[2].toLowerCase())));
				OptionsFrame.txtLeft.setText(((splits[3].toLowerCase())));
				OptionsFrame.txtDown.setText(((splits[4].toLowerCase())));
				OptionsFrame.txtRight.setText(((splits[5].toLowerCase())));
				OptionsFrame.txtOpen.setText(((splits[6].toLowerCase())));
				OptionsFrame.txtAttack.setText(((splits[7].toLowerCase())));
				OptionsFrame.txtNextCharac.setText(((splits[8].toLowerCase())));
				OptionsFrame.txtCharac1.setText(((splits[9].toLowerCase())));
				OptionsFrame.txtCharac2.setText(((splits[10].toLowerCase())));
				OptionsFrame.txtCharac3.setText(((splits[11].toLowerCase())));
				OptionsFrame.txtCharac4.setText(((splits[12].toLowerCase())));
				GameWorldFrame.score = Integer.parseInt(splits[13]);
				scoreOnLoad = Integer.parseInt(splits[13]);

			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block

		}

	}

	public void deleteProfile(boolean delete) {
		// Deleting the profile
		int selection = ProfileList.getSelectedIndex();
		profileName = (String) ProfileList.getSelectedValue();
		try {

			updateNameFile(delete);

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		File file = new File(Config.pathForEditable("Profiles/Profile_" + profileName + ".txt"));

		if (deleteConfirm == 0) {
			// deleting the file and removing it from the vector, then updating
			// the list.
			file.delete();

			if (selection >= 0) {

				VecProfiles.removeElementAt(selection);
				ProfileList.setListData(VecProfiles);
				listScrollPane.revalidate();
				listScrollPane.repaint();

				// select the next item

				if (selection >= VecProfiles.size())
					selection = VecProfiles.size() - 1;
				ProfileList.setSelectedIndex(selection);

			}

		}
	}

	public void updateNameFile(boolean delete) throws IOException {
		// Updating the name file, creating a new temp file.
		profileName = (String) ProfileList.getSelectedValue();
		String desiredName = nameField.getText();

		File inputFile = new File(profileNamesPath);
		File tempFile = new File("myTempFile.txt");

		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

		String lineToUpdate = profileName + " ";
		String updatedLine = desiredName + " " + GameWorldFrame.score;
		String currentLine;

		while ((currentLine = reader.readLine()) != null) {
			// trim newline when comparing with lineToRemove
			String trimmedLine = currentLine.trim();
			// if delete is false, it rewrites the line of the profile that is
			// being saved over.
			if (delete == false) {
				if (currentLine.contains(lineToUpdate)) {

					writer.write(updatedLine + System.getProperty("line.separator"));

				} else {

					writer.write(currentLine);
					writer.newLine();
				}
				// if delete is true, it completely removes the line.
			} else {

				if (trimmedLine.contains(lineToUpdate))
					continue;

				writer.write(currentLine + System.getProperty("line.separator"));

			}

		}
		writer.close();
		reader.close();
		// swaps the old file with the temp one.
		boolean successful = tempFile.renameTo(inputFile);
	}

	public void playGame() {
		// Initialising the world.
		setFocusable(false);
		setVisible(false);

		GameWorld.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GameWorld.window.getContentPane().removeAll();
		GameWorld.window.getContentPane().add(GameWorld.gameWorldFrame);
		GameWorld.window.validate();
		GameWorld.gameWorldFrame.repaint();
		GameWorld.window.setSize(480, 500);
		GameWorld.window.setVisible(true);
		GameWorld.window.setResizable(false);
		GameWorld.window.setLayout(null);

		OptionsFrame.getControls();
		try {

			GameWorld.gameWorldFrame.init();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void valueChanged(ListSelectionEvent event) {
		// checking if the value has changed in the lest, and reseting the
		// nameField text to the new selection
		if (event.getSource() == ProfileList && !event.getValueIsAdjusting()) {

			String stringValue = (String) ProfileList.getSelectedValue();
			if (stringValue != null)
				nameField.setText(stringValue);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
}
