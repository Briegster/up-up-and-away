package Items;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import Config.Config;

public class Coin {
	int height = 1;
	int width = 1;
	public int cornerx;
	public int cornery;

	ImageIcon healthItemIcon = new ImageIcon(Config.pathForStatic("Images/coin.gif"));

	public Coin(int x, int y) {
		this.cornerx = x * 16;
		this.cornery = y * 16;
		// TODO Auto-generated constructor stub
	}

	public Rectangle getCoinBounds() {
		return new Rectangle(cornerx, cornery, 16, 16);

	}

	public void draw(Graphics2D p, int startx, int starty) {

		p.drawImage(healthItemIcon.getImage(), cornerx + startx, cornery + starty, null);

	}

}
