package Items;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import Config.Config;

public class ShieldItemClass {
	int height = 1;
	int width = 1;
	public int cornerx;
	public int cornery;
	public static int shieldTimer;
	public static boolean shieldBoolean = false;

	ImageIcon shieldItemIcon = new ImageIcon(Config.pathForStatic("Images/shieldAni.gif"));

	public ShieldItemClass(int x, int y) {
		this.cornerx = x * 16;
		this.cornery = y * 16;

	}

	public Rectangle getShieldItemBounds() {
		return new Rectangle(cornerx, cornery, 16, 16);

	}

	public void draw(Graphics2D p, int startx, int starty) {
		// TODO Auto-generated method stub

		p.drawImage(shieldItemIcon.getImage(), cornerx + startx, cornery + starty, null);

	}
}
