package Items;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import Config.Config;

public class Key {
	int height = 1;
	int width = 1;
	public int cornerx;
	public int cornery;

	ImageIcon keyIcon = new ImageIcon(Config.pathForStatic("Images/keyIcon.gif"));

	public Key(int x, int y) {
		this.cornerx = x * 16;
		this.cornery = y * 16;
	}

	public Rectangle getKeyBounds() {
		return new Rectangle(cornerx, cornery, 16, 16);

	}

	public void draw(Graphics2D p, int startx, int starty) {
		// TODO Auto-generated method stub

		p.drawImage(keyIcon.getImage(), cornerx + startx, cornery + starty, null);

	}
}
