package Frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.sound.sampled.FloatControl;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Audio.BackgroundMusic;
import Audio.SFX;
import Config.Config;
import GameWorld.GameWorld;

public class OptionsFrame extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JTextField txtJump;
	public static JTextField txtLeft;
	public static JTextField txtDown;
	public static JTextField txtRight;
	public static JTextField txtOpen;
	public static JTextField txtAttack;
	public static JTextField txtNextCharac;
	public static JTextField txtCharac1;
	public static JTextField txtCharac2;
	public static JTextField txtCharac3;
	public static JTextField txtCharac4;
	public static JSlider musicSlider;

	public static char jumpChar;
	public static char leftChar;
	public static char downChar;
	public static char rightChar;
	public static char openChar;
	public static char attackChar;
	public static char nextCharacChar;
	public static char Charac1Char;
	public static char Charac2Char;
	public static char Charac3Char;
	public static char Charac4Char;

	static double musicVol = 0.5;
	public static double SFXVol = 0.5;
	public static ArrayList<Character> arrCharControls = new ArrayList<Character>();

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	OptionsFrame() {

		setForeground(Color.BLUE);

		txtJump = new JTextField();
		txtJump.setToolTipText("Jump key");
		txtJump.setBounds(16, 99, 30, 30);
		txtJump.setText("w");
		add(txtJump);

		txtJump.setColumns(10);

		txtLeft = new JTextField();
		txtLeft.setToolTipText("Left Key");
		txtLeft.setBounds(16, 141, 30, 30);
		txtLeft.setText("a");
		add(txtLeft);

		txtLeft.setColumns(10);

		txtDown = new JTextField();
		txtDown.setToolTipText("Down key");
		txtDown.setBounds(16, 183, 30, 30);
		txtDown.setText("s");
		add(txtDown);

		txtDown.setColumns(10);

		txtRight = new JTextField();
		txtRight.setToolTipText("Right key");
		txtRight.setBounds(16, 225, 30, 30);
		txtRight.setText("d");

		add(txtRight);

		txtRight.setColumns(10);

		txtOpen = new JTextField();
		txtOpen.setToolTipText("Open gate key");
		txtOpen.setBounds(16, 267, 30, 30);
		txtOpen.setText("e");
		add(txtOpen);

		txtOpen.setColumns(10);

		txtAttack = new JTextField();
		txtAttack.setToolTipText("Shoot key (_ is space)");
		txtAttack.setBounds(16, 309, 30, 30);
		txtAttack.setText("_");
		add(txtAttack);

		txtAttack.setColumns(10);

		txtNextCharac = new JTextField();
		txtNextCharac.setToolTipText("Next character key");
		txtNextCharac.setBounds(216, 100, 30, 30);
		txtNextCharac.setText("q");
		add(txtNextCharac);

		txtNextCharac.setColumns(10);

		txtCharac1 = new JTextField();
		txtCharac1.setToolTipText("First character key");
		txtCharac1.setBounds(216, 142, 30, 30);
		txtCharac1.setText("1");
		add(txtCharac1);

		txtCharac1.setColumns(10);

		txtCharac2 = new JTextField();
		txtCharac2.setToolTipText("Second character key");
		txtCharac2.setBounds(216, 184, 30, 30);
		txtCharac2.setText("2");

		add(txtCharac2);

		txtCharac2.setColumns(10);

		txtCharac3 = new JTextField();
		txtCharac3.setToolTipText("Third character key");
		txtCharac3.setBounds(216, 226, 30, 30);
		txtCharac3.setText("3");
		add(txtCharac3);

		txtCharac3.setColumns(10);

		txtCharac4 = new JTextField();
		txtCharac4.setToolTipText("Fourth character key");
		txtCharac4.setBounds(216, 268, 30, 30);
		txtCharac4.setText("4");
		add(txtCharac4);

		// Charac4Char = (txtCharac4.getText()).charAt(1);
		txtCharac4.setColumns(10);

		JSlider musicSlider = new JSlider();
		musicSlider.setToolTipText("Set music volume");
		musicSlider.setBounds(16, 405, 190, 29);

		ChangeListener musicChangeListener = new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent ce) {
				JSlider musicSlider = (JSlider) ce.getSource();
				if (!musicSlider.getValueIsAdjusting())

					musicVol = musicSlider.getValue() / 100.0;

				getVolume();

			}

		};
		musicSlider.addChangeListener(musicChangeListener);

		JSlider soundEffectsSlider = new JSlider();
		soundEffectsSlider.setToolTipText("Set SFX volume");
		soundEffectsSlider.setBounds(16, 465, 190, 29);
		ChangeListener SFXChangeListener = new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent ce) {
				JSlider soundEffectsSlider = (JSlider) ce.getSource();
				if (!soundEffectsSlider.getValueIsAdjusting())

					SFXVol = soundEffectsSlider.getValue() / 100.0;

				getVolume();

			}

		};
		soundEffectsSlider.addChangeListener(SFXChangeListener);

		JLabel lblJump = new JLabel("Jump");
		lblJump.setForeground(Color.WHITE);
		lblJump.setBounds(58, 106, 61, 16);

		JLabel lblMoveLeft = new JLabel("Move Left");
		lblMoveLeft.setForeground(Color.WHITE);
		lblMoveLeft.setBounds(58, 148, 61, 16);

		JLabel lblMoveDown = new JLabel("Move Down");
		lblMoveDown.setForeground(Color.WHITE);
		lblMoveDown.setBounds(58, 190, 87, 16);

		JLabel lblMoveRight = new JLabel("Move Right");
		lblMoveRight.setForeground(Color.WHITE);
		lblMoveRight.setBounds(58, 232, 87, 16);

		JLabel lblOpenGate = new JLabel("Open Gate");
		lblOpenGate.setForeground(Color.WHITE);
		lblOpenGate.setBounds(58, 274, 87, 16);

		JLabel lblShoot = new JLabel("Shoot");
		lblShoot.setForeground(Color.WHITE);
		lblShoot.setBounds(58, 316, 61, 16);

		JLabel lblNextCharacter = new JLabel("Next Character");
		lblNextCharacter.setForeground(Color.WHITE);
		lblNextCharacter.setBounds(258, 106, 105, 16);

		JLabel lblFirstCharacter = new JLabel("First Character");
		lblFirstCharacter.setForeground(Color.WHITE);
		lblFirstCharacter.setBounds(258, 148, 105, 16);

		JLabel lblSecondCharacter = new JLabel("Second Character");
		lblSecondCharacter.setForeground(Color.WHITE);
		lblSecondCharacter.setBounds(258, 190, 117, 16);

		JLabel lblThirdCharacter = new JLabel("Third Character");
		lblThirdCharacter.setForeground(Color.WHITE);
		lblThirdCharacter.setBounds(258, 232, 105, 16);

		JLabel lblForthCharacter = new JLabel("Forth Character");
		lblForthCharacter.setForeground(Color.WHITE);
		lblForthCharacter.setBounds(258, 274, 105, 16);

		JLabel lblMusicVolume = new JLabel("Music Volume");
		lblMusicVolume.setForeground(Color.WHITE);
		lblMusicVolume.setBounds(216, 410, 88, 16);

		JLabel lblSoundEffectsVolume = new JLabel("Sound Effects Volume");
		lblSoundEffectsVolume.setForeground(Color.WHITE);
		lblSoundEffectsVolume.setBounds(216, 468, 147, 16);

		JButton btnBack = new JButton("Back");
		btnBack.setToolTipText("Return to Menu");
		btnBack.setBounds(16, 16, 73, 29);
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Options.window.setVisible(false);

				GameWorld.menuFrame.setVisible(true);
			}
		});
		btnBack.setIcon(new ImageIcon(Config.pathForStatic("Images/backIcon.gif")));
		setLayout(null);

		add(musicSlider);
		add(soundEffectsSlider);
		add(lblJump);
		add(lblMoveLeft);
		add(lblMoveDown);
		add(lblMoveRight);
		add(lblOpenGate);
		add(lblShoot);
		add(lblNextCharacter);
		add(lblFirstCharacter);
		add(lblSecondCharacter);
		add(lblThirdCharacter);
		add(lblForthCharacter);
		add(lblMusicVolume);
		add(lblSoundEffectsVolume);
		add(btnBack);

		JLabel lblControls = new JLabel("Controls:");
		lblControls.setForeground(Color.WHITE);
		lblControls.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblControls.setBounds(16, 71, 98, 23);
		add(lblControls);

		JLabel lblSound = new JLabel("Sound:");
		lblSound.setForeground(Color.WHITE);
		lblSound.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblSound.setBounds(16, 375, 98, 23);
		add(lblSound);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Config.pathForStatic("Images/OptionsBackGround.jpg")));
		lblNewLabel.setBounds(0, 0, 500, 500);
		add(lblNewLabel);

	}

	public static void getVolume() {
		// setting the volume based on the position of the sliders
		try {
			FloatControl musicVolume = (FloatControl) BackgroundMusic.clip.getControl(FloatControl.Type.MASTER_GAIN);
			double musicGain = musicVol;
			float musicdB = (float) (Math.log(musicGain) / Math.log(10.0) * 20.0);
			musicVolume.setValue(musicdB);

			FloatControl SFXVolume = (FloatControl) SFX.clip.getControl(FloatControl.Type.MASTER_GAIN);
			double SFXGain = SFXVol;
			float SFXdB = (float) (Math.log(SFXGain) / Math.log(10.0) * 20.0);

			SFXVolume.setValue(SFXdB);

		} catch (Exception e) {

		}

	}

	public static void getControls() {
		// setting the controls to their corresponding text field
		jumpChar = (txtJump.getText()).charAt(0);
		leftChar = (txtLeft.getText()).charAt(0);
		downChar = (txtDown.getText()).charAt(0);
		rightChar = (txtRight.getText()).charAt(0);
		openChar = (txtOpen.getText()).charAt(0);
		if ((txtAttack.getText()).charAt(0) == ("_").charAt(0)) {

			attackChar = (" ").charAt(0);
		} else {
			attackChar = (txtAttack.getText()).charAt(0);
		}

		nextCharacChar = (txtNextCharac.getText()).charAt(0);
		Charac1Char = (txtCharac1.getText()).charAt(0);
		Charac2Char = (txtCharac2.getText()).charAt(0);
		Charac3Char = (txtCharac3.getText()).charAt(0);
		Charac4Char = (txtCharac4.getText()).charAt(0);
		// reseting the controls array
		arrCharControls.clear();
		arrCharControls.add(jumpChar);
		arrCharControls.add(leftChar);
		arrCharControls.add(downChar);
		arrCharControls.add(rightChar);
		arrCharControls.add(openChar);
		arrCharControls.add(attackChar);
		arrCharControls.add(nextCharacChar);

	}
}
