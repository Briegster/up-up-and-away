package Frames;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Config.Config;
import GameWorld.GameWorld;

public class HighScoresFrame extends JPanel {

	private JPanel contentPane;
	private static JTable table = new JTable();

	static Map<Integer, String> unsortedMap = new HashMap<Integer, String>();
	static ArrayList<Integer> arrScores = new ArrayList<Integer>();
	public static ArrayList<String> arrNames = new ArrayList<String>();
	Map<Integer, String> treeMap = new TreeMap<Integer, String>();
	static boolean init = false;

	@SuppressWarnings({ "rawtypes", "deprecation" })
	public HighScoresFrame() {
		setLayout(null);

		JScrollPane HighscoresScrollPane = new JScrollPane();
		HighscoresScrollPane.setBounds(50, 50, 380, 400);
		add(HighscoresScrollPane);

		HighscoresScrollPane.setViewportView(table);

		JButton btnBack = new JButton("Back");
		btnBack.setToolTipText("Return to Menu");
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				HighScoresInit.window.setVisible(false);

				GameWorld.menuFrame.setVisible(true);
			}
		});
		btnBack.setIcon(new ImageIcon(Config.pathForStatic("Images/backIcon.gif")));
		btnBack.setBounds(6, 6, 76, 29);
		add(btnBack);

		JLabel lblBackground = new JLabel("");
		lblBackground.setIcon(new ImageIcon(Config.pathForStatic("Images/HighscoresBackground.jpg")));
		lblBackground.setBounds(0, 0, 480, 480);
		add(lblBackground);

		table.enable(false);
	}

	public static void getProfiles() {
		// getting the profile names and their scores and adding them to the
		// array.
		File f = new File(Config.pathForEditable("Profiles/ProfileNames.txt"));

		arrScores.clear();
		arrNames.clear();
		unsortedMap.clear();

		Scanner scanner;
		try {

			scanner = new Scanner(f);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] splits = line.split(" ");

				if (splits.length != 2) {

					continue;

				}
				arrNames.add(splits[0]);
				arrScores.add(Integer.parseInt(splits[1]));

			}
		} catch (FileNotFoundException e) {

		}
		bubble();

		init = true;
	}

	public static void bubble() {
		// sorting the array of scores, and swapping them and their names
		int last = arrScores.size();
		boolean swapped = true;
		while (swapped == true) {
			swapped = false;
			int i = 0;
			while (i < last) {
				try {
					if (arrScores.get(i) < arrScores.get(i + 1)) {

						int tmpInt = arrScores.get(i + 1);
						arrScores.set(i + 1, arrScores.get(i));
						arrScores.set(i, tmpInt);

						String tmpStr = arrNames.get(i + 1);
						arrNames.set(i + 1, arrNames.get(i));
						arrNames.set(i, tmpStr);

						swapped = true;
					}
				} catch (IndexOutOfBoundsException e) {

				}
				i++;
			}
			last--;
		}
		map();
	}

	public static void map() {

		// adding the sorted arrays into the table

		Object[][] arr = new Object[arrScores.size()][2];

		for (int i = 0; i < arrScores.size(); i++) {

			arr[i][0] = arrScores.get(i);
			arr[i][1] = arrNames.get(i);

		}

		String[] columnNames = { "Score", "Profile" };
		DefaultTableModel tableModel = new DefaultTableModel(arr, columnNames);
		table.setModel(tableModel);
		tableModel.fireTableDataChanged();

	}

}
