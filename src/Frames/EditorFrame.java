package Frames;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import Config.Config;
import GameWorld.GameWorld;
import GameWorld.GameWorldFrame;
import GameWorld.LevelGrid;

public class EditorFrame extends JPanel implements ActionListener, MouseListener, MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int size = 16;
	int startX = 112;
	int startY = 48;
	static int width = 60;
	static int height = 40;
	int levelInput = 0;
	int numOfChapters = 3;

	String saveInput;
	String fileName;
	public static String helpMessage = "null";
	static String loadFileName;
	int chapter = 0;
	static String[][] grid = new String[width][height];
	boolean mouseDragged = false;

	boolean clear = false;
	static boolean loading = false;

	boolean shownChapterMsg = false;
	boolean levelsLoaded = false;
	boolean loadBtnPressed = false;
	boolean outOfLevelsPanel = false;

	public static boolean testingLevel = false;
	int tmplevel;
	int tmpScore;

	JPanel levelsPanel;
	Timer paintTimer;

	JButton btnBackground;
	JButton btnPlatform;
	JButton btnClear;
	JButton btnChangeChapter;
	JButton btnFillAll;

	Rectangle gridRect = new Rectangle(startX, startY, width * 16, height * 16);

	String waterString = new String("water");
	String platformString = new String("platform");
	String stoneString = new String("stone");
	String backgroundString = new String("");

	String currentImageString = platformString;

	String currentPlatString = platformString;

	String cloudString = new String("cloud");
	String spikeString = new String("spike");
	String movingSpikeString = new String("movingSpike");
	String jumpBoostString = new String("jumpBoostItem");
	String healthString = new String("healthItem");
	String shieldString = new String("shieldItem");
	String gateString = new String("gate");
	String keyString = new String("key");
	String coinString = new String("coin");

	String louDoorString = new String("LouDoor");
	String brickDoorString = new String("BrickDoor");
	String steveDoorString = new String("SteveDoor");
	String andyDoorString = new String("AndyDoor");
	String brickSpawnString = new String("brickSpawn");
	String louSpawnString = new String("louSpawn");
	String steveSpawnString = new String("steveSpawn");
	String andySpawnString = new String("andySpawn");

	ImageIcon dirtPlatTileIcon = new ImageIcon(Config.pathForStatic("Images/dirttile.gif"));
	ImageIcon dirtTileIcon = new ImageIcon(Config.pathForStatic("Images/backdirttile.jpg"));
	ImageIcon currentBackIcon = dirtTileIcon;
	ImageIcon currentPlatIcon = dirtPlatTileIcon;

	ImageIcon skyTileIcon = new ImageIcon(Config.pathForStatic("Images/skytile.jpg"));
	ImageIcon cloudTileIcon = new ImageIcon(Config.pathForStatic("Images/cloud.gif"));
	ImageIcon spikeTileIcon = new ImageIcon(Config.pathForStatic("Images/spike.gif"));
	ImageIcon waterTileIcon = new ImageIcon(Config.pathForStatic("Images/waterAni.gif"));
	ImageIcon stoneTileIcon = new ImageIcon(Config.pathForStatic("Images/stone.gif"));
	ImageIcon stonePlatTileIcon = new ImageIcon(Config.pathForStatic("Images/stonePlatform.gif"));

	ImageIcon movingSpikeIcon = new ImageIcon(Config.pathForStatic("Images/movingSpike.gif"));
	ImageIcon healthItemIcon = new ImageIcon(Config.pathForStatic("Images/healthAni.gif"));
	ImageIcon jumpBoostIcon = new ImageIcon(Config.pathForStatic("Images/jumpBoostAni.gif"));
	ImageIcon shieldItemIcon = new ImageIcon(Config.pathForStatic("Images/shieldAni.gif"));
	ImageIcon gateIcon = new ImageIcon(Config.pathForStatic("Images/gateClosed.gif"));
	ImageIcon keyIcon = new ImageIcon(Config.pathForStatic("Images/keyIcon.gif"));
	ImageIcon coinIcon = new ImageIcon(Config.pathForStatic("Images/coin.gif"));

	ImageIcon louSpawnIcon = new ImageIcon(Config.pathForStatic("Images/louSpawn.gif"));
	ImageIcon brickSpawnIcon = new ImageIcon(Config.pathForStatic("Images/BrickSpawn.gif"));
	ImageIcon steveSpawnIcon = new ImageIcon(Config.pathForStatic("Images/steveSpawn.gif"));
	ImageIcon andySpawnIcon = new ImageIcon(Config.pathForStatic("Images/andySpawn.gif"));
	ImageIcon steveDoorIcon = new ImageIcon(Config.pathForStatic("Images/steveDoor.gif"));
	ImageIcon brickDoorIcon = new ImageIcon(Config.pathForStatic("Images/BrickDoor.gif"));
	ImageIcon louDoorIcon = new ImageIcon(Config.pathForStatic("Images/louDoor.gif"));
	ImageIcon andyDoorIcon = new ImageIcon(Config.pathForStatic("Images/andyDoor.gif"));

	Vector<String> vecLevels = new Vector<String>();

	DefaultComboBoxModel model = new DefaultComboBoxModel(vecLevels);
	JComboBox levelsComboBox = new JComboBox(model);

	public EditorFrame() {
		addMouseListener(this);
		addMouseMotionListener(this);
		setLayout(null);
		paintTimer = new Timer(30, this);
		paintTimer.start();

		gatherLevels();
		final JPanel levelsPanel = new JPanel();
		levelsPanel.setBackground(new Color(0, 153, 255));
		levelsPanel.setBounds(0, 72, 95, 27);
		add(levelsPanel);
		levelsPanel.setVisible(false);
		levelsPanel.setLayout(null);

		btnBackground = new JButton("");
		btnBackground.setToolTipText("Background");

		btnBackground.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// setting the currentimagestring to be the background.

				currentImageString = backgroundString;

			}
		});

		btnBackground.setIcon(dirtTileIcon);
		btnBackground.setBounds(6, 156, 26, 26);
		add(btnBackground);

		JButton btnSave = new JButton("Save");
		btnSave.setToolTipText("Save the level");
		btnSave.setIcon(new ImageIcon(Config.pathForStatic("Images/saveIcon.jpg")));
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// checking for a spawn

				if (doorCheck()) {
					levelSaver();
				}
			}

		});
		btnSave.setBounds(6, 633, 95, 26);
		add(btnSave);

		btnPlatform = new JButton("");
		btnPlatform.setToolTipText("Platform");
		btnPlatform.setIcon(dirtPlatTileIcon);
		btnPlatform.setBounds(6, 115, 26, 26);
		add(btnPlatform);
		btnPlatform.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				currentImageString = platformString;

			}
		});

		btnChangeChapter = new JButton("Chapter");
		btnChangeChapter.setIcon(new ImageIcon(Config.pathForStatic("Images/stone.gif")));
		btnChangeChapter.setToolTipText("Change the chapter");
		btnChangeChapter.setBounds(6, 511, 95, 29);
		add(btnChangeChapter);
		btnChangeChapter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// changing the chapter
				chapter = (chapter += 1) % numOfChapters;
				changeChapter();
			}
		});

		JButton btnCloud = new JButton("");
		btnCloud.setToolTipText("Cloud");
		btnCloud.setIcon(cloudTileIcon);
		btnCloud.setBounds(6, 197, 26, 26);
		add(btnCloud);
		btnCloud.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				currentImageString = cloudString;

			}
		});

		JButton btnSpike = new JButton("");
		btnSpike.setToolTipText("Spike");
		btnSpike.setIcon(spikeTileIcon);

		btnSpike.setBounds(6, 238, 26, 26);
		add(btnSpike);

		JButton btnMovingSpike = new JButton("");
		btnMovingSpike.setToolTipText("Moving Spike");
		btnMovingSpike.setIcon(new ImageIcon(Config.pathForStatic("Images/movingSpike.gif")));
		btnMovingSpike.setBounds(6, 284, 26, 26);
		add(btnMovingSpike);

		JButton btnHealth = new JButton("");
		btnHealth.setToolTipText("Health");
		btnHealth.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentImageString = healthString;

			}
		});
		btnHealth.setIcon(new ImageIcon(Config.pathForStatic("Images/healthAni.gif")));
		btnHealth.setBounds(44, 156, 26, 26);
		add(btnHealth);

		JButton btnJumpItem = new JButton("");
		btnJumpItem.setToolTipText("Jump Boost");
		btnJumpItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = jumpBoostString;

			}
		});
		btnJumpItem.setIcon(new ImageIcon(Config.pathForStatic("Images/jumpBoostAni.gif")));
		btnJumpItem.setBounds(44, 197, 26, 26);
		add(btnJumpItem);

		JButton btnLouDoor = new JButton("");
		btnLouDoor.setToolTipText("Lou Door");
		btnLouDoor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentImageString = louDoorString;

			}
		});
		btnLouDoor.setIcon(new ImageIcon(Config.pathForStatic("Images/LouDoor.gif")));
		btnLouDoor.setBounds(44, 360, 26, 26);
		add(btnLouDoor);

		JButton btnGate = new JButton("");
		btnGate.setToolTipText("Gate");
		btnGate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = gateString;

			}
		});
		btnGate.setIcon(new ImageIcon(Config.pathForStatic("Images/gateClosed.gif")));
		btnGate.setBounds(44, 238, 26, 26);
		add(btnGate);

		JButton btnBack = new JButton("Back");
		btnBack.setToolTipText("Return to Menu");
		btnBack.setIcon(new ImageIcon(Config.pathForStatic("Images/backIcon.gif")));
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// reseting the game back to what it was before the user
				// attempted to test a level and returning to the menu.
				if (testingLevel == true) {
					LevelGrid.erase();
					GameWorldFrame.started = false;
					LevelGrid.levelNum = tmplevel;

					GameWorld.gameWorldFrame.score = tmpScore;
					testingLevel = false;
				}

				Editor.window.setVisible(false);

				GameWorld.menuFrame.setVisible(true);
			}
		});
		btnBack.setBounds(6, 6, 70, 29);
		add(btnBack);

		btnClear = new JButton("Clear All");
		btnClear.setIcon(new ImageIcon(Config.pathForStatic("Images/backdirttile.jpg")));
		btnClear.setToolTipText("Set all tiles to background");
		btnClear.addActionListener(new ActionListener() {
			@Override
			// chekcing if they are sure they want to clear the map, and thn
			// clearing it.
			public void actionPerformed(ActionEvent e) {
				int sure = 1;
				sure = JOptionPane.showConfirmDialog(getParent(), "Are you sure you want to clear?");
				if (sure == 0) {
					initOrErase();
				}

			}
		});
		btnClear.setBounds(6, 538, 95, 29);
		add(btnClear);

		JButton btnLoad = new JButton("Load");
		btnLoad.setToolTipText("Load a level file");
		btnLoad.setIcon(new ImageIcon(Config.pathForStatic("Images/LoadIcon.gif")));
		btnLoad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// showing or hiding the load levels panel

				if (loadBtnPressed == false) {
					levelsPanel.setVisible(true);

					loadBtnPressed = true;
				} else {
					levelsPanel.setVisible(false);
					loadBtnPressed = false;
				}

			}
		});
		btnLoad.setBounds(5, 41, 95, 29);
		add(btnLoad);

		JButton btnKey = new JButton("");
		btnKey.setToolTipText("Key");
		btnKey.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentImageString = keyString;

			}
		});
		btnKey.setIcon(new ImageIcon(Config.pathForStatic("Images/keyIcon.gif")));
		btnKey.setBounds(44, 284, 26, 26);
		add(btnKey);

		btnFillAll = new JButton("Fill All");
		btnFillAll.setIcon(new ImageIcon(Config.pathForStatic("Images/dirttile.gif")));
		btnFillAll.setToolTipText("Set all tiles to platforms");
		btnFillAll.addActionListener(new ActionListener() {
			// checking if they are sure they want to fill, then filling it.
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int sure = 1;
				sure = JOptionPane.showConfirmDialog(getParent(), "Are you sure you want to fill all with platforms?");
				if (sure == 0) {
					for (int x = 0; x < width; x++) {
						for (int y = 0; y < height; y++) {

							grid[x][y] = platformString;
							repaint();
						}
					}
				}

			}

		});
		btnFillAll.setBounds(6, 567, 95, 29);
		add(btnFillAll);

		JButton btnLouSpawn = new JButton("");
		btnLouSpawn.setToolTipText("Lou Spawn");
		btnLouSpawn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = louSpawnString;

			}
		});
		btnLouSpawn.setIcon(new ImageIcon(Config.pathForStatic("Images/louSpawn.gif")));
		btnLouSpawn.setBounds(6, 360, 26, 26);
		add(btnLouSpawn);

		JButton btnCoin = new JButton("");
		btnCoin.setToolTipText("Coin");
		btnCoin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = coinString;

			}
		});
		btnCoin.setIcon(new ImageIcon(Config.pathForStatic("Images/coin.gif")));
		btnCoin.setBounds(44, 322, 26, 26);
		add(btnCoin);

		JButton btnBrickSpawn = new JButton("");
		btnBrickSpawn.setToolTipText("Brick Spawn");
		btnBrickSpawn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = brickSpawnString;

			}
		});
		btnBrickSpawn.setIcon(new ImageIcon(Config.pathForStatic("Images/BrickSpawn.gif")));
		btnBrickSpawn.setBounds(6, 436, 26, 26);
		add(btnBrickSpawn);

		JButton btnSteveSpawn = new JButton("");
		btnSteveSpawn.setToolTipText("Steve Spawn");
		btnSteveSpawn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = steveSpawnString;

			}
		});
		btnSteveSpawn.setIcon(new ImageIcon(Config.pathForStatic("Images/steveSpawn.gif")));
		btnSteveSpawn.setBounds(6, 473, 26, 26);
		add(btnSteveSpawn);

		JButton btnAndyDoor = new JButton("");
		btnAndyDoor.setToolTipText("Andy Door");
		btnAndyDoor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentImageString = andyDoorString;

			}
		});
		btnAndyDoor.setIcon(new ImageIcon(Config.pathForStatic("Images/andyDoor.gif")));
		btnAndyDoor.setBounds(44, 398, 26, 26);
		add(btnAndyDoor);

		JButton btnWater = new JButton("");
		btnWater.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = waterString;

			}
		});
		btnWater.setIcon(new ImageIcon(Config.pathForStatic("Images/waterAni.gif")));
		btnWater.setToolTipText("Water");
		btnWater.setBounds(6, 322, 26, 26);
		add(btnWater);

		JButton btnAndySpawn = new JButton("");
		btnAndySpawn.setToolTipText("Andy Spawn");
		btnAndySpawn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentImageString = andySpawnString;

			}
		});
		btnAndySpawn.setIcon(new ImageIcon(Config.pathForStatic("Images/andySpawn.gif")));
		btnAndySpawn.setBounds(6, 398, 26, 26);
		add(btnAndySpawn);

		JButton btnShield = new JButton("");
		btnShield.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = shieldString;

			}
		});
		btnShield.setToolTipText("Shield");
		btnShield.setIcon(new ImageIcon(Config.pathForStatic("Images/shieldAni.gif")));
		btnShield.setBounds(44, 115, 26, 26);
		add(btnShield);

		JButton btnBrickDoor = new JButton("");
		btnBrickDoor.setToolTipText("Brick Door");
		btnBrickDoor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentImageString = brickDoorString;

			}
		});
		btnBrickDoor.setIcon(new ImageIcon(Config.pathForStatic("Images/brickDoor.gif")));
		btnBrickDoor.setBounds(44, 436, 26, 26);
		add(btnBrickDoor);

		JButton btnSteveDoor = new JButton("");
		btnSteveDoor.setToolTipText("Steve Door");
		btnSteveDoor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentImageString = steveDoorString;

			}
		});
		btnSteveDoor.setIcon(new ImageIcon(Config.pathForStatic("Images/steveDoor.gif")));
		btnSteveDoor.setBounds(44, 473, 26, 26);
		add(btnSteveDoor);
		levelsComboBox.setToolTipText("Load a level");

		levelsComboBox.setBounds(4, 0, 95, 27);
		levelsPanel.add(levelsComboBox);
		levelsComboBox.addActionListener(new ActionListener() {
			// loading a level in.
			@Override
			public void actionPerformed(ActionEvent e) {
				levelInput = (Integer.parseInt((String) levelsComboBox.getSelectedItem()));
				if (levelInput != 0) {

					loadFileName = Config.pathForEditable("Levels/level" + levelInput + ".txt");
					loading = true;
					initOrErase();
					loadfile(EditorFrame.loadFileName);
					changeChapter();
					loadBtnPressed = false;
					saveInput = (String) levelsComboBox.getSelectedItem();
				}
				levelsPanel.setVisible(false);
			}
		});

		btnMovingSpike.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = movingSpikeString;

			}
		});

		btnSpike.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentImageString = spikeString;

			}
		});

		JButton btnTestLevel = new JButton("Test Level");
		btnTestLevel.setToolTipText("Test this level");
		btnTestLevel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// saving the level as 777 and then playing it.
				saveInput = "777";
				testingLevel = true;
				tmplevel = LevelGrid.levelNum;
				tmpScore = GameWorld.gameWorldFrame.score;
				LevelGrid.levelNum = 777;

				levelSaver();
				try {
					if (LevelGrid.helpMessage.equals(helpMessage)) {
						helpMessage = "null";
					}
				} catch (NullPointerException e) {
					helpMessage = "null";
				}

				LevelGrid.helpMessage = helpMessage;
				if (doorCheck()) {
					testLevel();
				}

			}
		});
		btnTestLevel.setIcon(new ImageIcon(Config.pathForStatic("Images/PlayIcon.gif")));
		btnTestLevel.setBounds(6, 599, 95, 29);
		add(btnTestLevel);

		JLabel lblBackGroundImage = new JLabel("");
		lblBackGroundImage.setIcon(new ImageIcon(Config.pathForStatic("Images/EditorBackground.jpg")));
		lblBackGroundImage.setBounds(0, 0, 1200, 800);
		add(lblBackGroundImage);
	}

	public static void initOrErase() {
		// wiping the grid.
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				grid[x][y] = "";

			}

		}
	}

	public boolean doorCheck() {
		// checking if there are doors for the spawns.
		boolean louSpawn = false;
		boolean steveSpawn = false;
		boolean brickSpawn = false;
		boolean andySpawn = false;

		boolean louDoor = false;
		boolean steveDoor = false;
		boolean brickDoor = false;
		boolean andyDoor = false;

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (grid[x][y].equals(louSpawnString)) {

					louSpawn = true;

				}
				if (grid[x][y].equals(steveSpawnString)) {

					steveSpawn = true;

				}
				if (grid[x][y].equals(brickSpawnString)) {

					brickSpawn = true;

				}
				if (grid[x][y].equals(andySpawnString)) {

					andySpawn = true;

				}
				if (grid[x][y].equals(louDoorString)) {

					louDoor = true;

				}
				if (grid[x][y].equals(steveDoorString)) {

					steveDoor = true;

				}
				if (grid[x][y].equals(brickDoorString)) {

					brickDoor = true;

				}
				if (grid[x][y].equals(andyDoorString)) {

					andyDoor = true;

				}

			}
		}

		if (louSpawn == false && steveSpawn == false && brickSpawn == false && andySpawn == false) {
			JOptionPane.showMessageDialog(null, "You must have at least one spawn and its matching door.");
			return (false);
		} else if (louSpawn == louDoor && steveSpawn == steveDoor && brickSpawn == brickDoor && andySpawn == andyDoor) {
			return (true);
		} else {

			JOptionPane.showMessageDialog(null, "You must have the appropriate amount of spawns and doors.");
			return (false);
		}
	}

	public void loadfile(String filename) {
		// loading a file and painting it to the grid.
		File f = new File(filename);

		Scanner scanner;
		try {

			scanner = new Scanner(f);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] splits = line.split(" ");

				if (splits.length != 3) {

					continue;

				}

				int tileX = Integer.parseInt(splits[0]);
				int tileY = Integer.parseInt(splits[1]);
				String tileType = splits[2];

				grid[tileX - 30][tileY - 20] = tileType;

				repaint();

			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			JOptionPane.showMessageDialog(getParent(), "File not found.");

		}
		// checking what the help message and chapter of the file is.

		File f1 = new File(Config.pathForEditable("Levels/levelNumber.txt"));
		Scanner scanner2;
		int countTo = 0;
		try {
			scanner2 = new Scanner(f1);
			while (scanner2.hasNextLine()) {
				String line = scanner2.nextLine();
				String[] splits = line.split(" ");

				if (splits.length != 3) {

					continue;

				}
				countTo++;
				if (countTo == levelInput) {
					chapter = Integer.parseInt(splits[2]);
					helpMessage = splits[1];

				}

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void changeChapter() {
		// changing the chapter
		if (chapter == 0) {

			btnBackground.setIcon(dirtTileIcon);
			btnPlatform.setIcon(dirtPlatTileIcon);
			btnClear.setIcon(dirtTileIcon);
			btnChangeChapter.setIcon(stoneTileIcon);
			btnFillAll.setIcon(dirtPlatTileIcon);
			currentBackIcon = dirtTileIcon;
			currentPlatIcon = dirtPlatTileIcon;

		} else if (chapter == 1) {

			btnBackground.setIcon(stoneTileIcon);
			btnPlatform.setIcon(stonePlatTileIcon);
			btnClear.setIcon(stoneTileIcon);
			btnChangeChapter.setIcon(skyTileIcon);
			btnFillAll.setIcon(stonePlatTileIcon);
			currentBackIcon = stoneTileIcon;
			currentPlatIcon = stonePlatTileIcon;

		} else if (chapter == 2) {
			btnBackground.setIcon(skyTileIcon);
			btnPlatform.setIcon(dirtPlatTileIcon);
			btnClear.setIcon(skyTileIcon);
			btnChangeChapter.setIcon(dirtTileIcon);
			btnFillAll.setIcon(dirtPlatTileIcon);
			currentBackIcon = skyTileIcon;
			currentPlatIcon = dirtPlatTileIcon;
		}

		repaint();
	}

	public void testLevel() {
		// testing the level, which resets everything in game and initializes
		// the game world.
		LevelGrid.erase();
		GameWorldFrame.started = false;
		GameWorld.gameWorldFrame.score = 0;
		Editor.window.setFocusable(false);
		Editor.window.setVisible(false);
		GameWorld.window.getContentPane().removeAll();
		GameWorld.window.getContentPane().add(GameWorld.gameWorldFrame);
		GameWorld.window.validate();
		GameWorld.gameWorldFrame.repaint();
		GameWorld.window.setSize(480, 500);
		GameWorld.window.setVisible(true);
		GameWorld.window.setResizable(false);
		GameWorld.window.getContentPane().setLayout(null);

		OptionsFrame.getControls();

		try {

			GameWorld.gameWorldFrame.init();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void paint(Graphics g) {
		// painting the world, depending of the string associated with that
		// point in the grid.
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.gray);
		g2d.fillRect(startX, startY, width * 16, height * 16);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				g2d.drawRect(x * size + startX, y * size + startY, size, size);

				if (grid[x][y].equals("")) {

					g2d.drawImage(currentBackIcon.getImage(), x * size + startX, y * size + startY, null);

				} else if (grid[x][y].equals(platformString)) {
					g2d.drawImage(currentPlatIcon.getImage(), (x * size) + 1 + startX, (y * size) + 1 + startY, null);
				} else if (grid[x][y].equals(stoneString)) {
					g2d.drawImage(stoneTileIcon.getImage(), (x * size) + startX, (y * size) + startY, null);

				} else if (grid[x][y].equals(waterString)) {
					g2d.drawImage(waterTileIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(cloudString)) {
					g2d.drawImage(cloudTileIcon.getImage(), (x * size) + startX, (y * size) + startY, null);

				} else if (grid[x][y].equals(spikeString)) {
					g2d.drawImage(spikeTileIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(movingSpikeString)) {
					g2d.drawImage(movingSpikeIcon.getImage(), (x * size) + startX, (y * size) + startY, null);

				} else if (grid[x][y].equals(jumpBoostString)) {
					g2d.drawImage(jumpBoostIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(healthString)) {
					g2d.drawImage(healthItemIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(shieldString)) {
					g2d.drawImage(shieldItemIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(keyString)) {
					g2d.drawImage(keyIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(gateString)) {
					g2d.drawImage(gateIcon.getImage(), (x * size) + startX, (y * size) + startY, null);

				} else if (grid[x][y].equals(coinString)) {
					g2d.drawImage(coinIcon.getImage(), (x * size) + startX, (y * size) + startY, null);

				} else if (grid[x][y].equals(brickSpawnString)) {
					g2d.drawImage(brickSpawnIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(steveSpawnString)) {
					g2d.drawImage(steveSpawnIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(louSpawnString)) {
					g2d.drawImage(louSpawnIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(andySpawnString)) {
					g2d.drawImage(andySpawnIcon.getImage(), (x * size) + startX, (y * size) + startY, null);

				} else if (grid[x][y].equals(louDoorString)) {
					g2d.drawImage(louDoorIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(brickDoorString)) {
					g2d.drawImage(brickDoorIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(steveDoorString)) {
					g2d.drawImage(steveDoorIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				} else if (grid[x][y].equals(andyDoorString)) {
					g2d.drawImage(andyDoorIcon.getImage(), (x * size) + startX, (y * size) + startY, null);
				}
				g2d.setColor(Color.black);
				g2d.drawRect((x * size) + 1 + startX, (y * size) + 1 + startY, size, size);

			}
		}
		repaint();

	}

	public void drawTile() {
		// determining the location of the mouse, and then changing the string
		// associated with the point on the grid, then repainting.
		PointerInfo a = MouseInfo.getPointerInfo();
		Point b = a.getLocation();
		int mouseX = (int) b.getX();
		int mouseY = (int) b.getY();
		int gMX = ((mouseX - 48) / 16) - 4;
		int gMY = ((mouseY - 48) / 16) - 3;

		grid[gMX][gMY] = currentImageString;

		repaint();
	}

	public void gatherLevels() {
		// gathering all of the available levels to load from and adding them to
		// the vector for the combobox.
		if (levelsLoaded == false) {

			levelsLoaded = true;
			File f = new File(Config.pathForEditable("Levels/levelNumber.txt"));
			Scanner scanner;

			try {

				scanner = new Scanner(f);
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					String[] splits = line.split(" ");

					if (splits.length != 3) {

						continue;

					}
					if (splits[0].equals("777")) {

					} else {
						vecLevels.add(splits[0]);
					}

				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

	}

	public void levelSaver() {
		// Checking what the user want to save their level as, doesn't prompt if
		// they are testing.
		int saveOver = 0;
		if (testingLevel == false) {

			saveInput = JOptionPane.showInputDialog(getParent(), "Save As: (Integer input only) src/Levels/level:");
		}

		if (saveInput != null) {
			try {
				// checking if the level exists, and prompting to save over.
				if (Integer.parseInt(saveInput) >= 0) {
					if (testingLevel == false) {
						for (int l = 0; l < vecLevels.size(); l++) {

							if (saveInput.equals(vecLevels.get(l))) {

								saveOver = JOptionPane.showConfirmDialog(getParent(), "That level already exists, are you sure you want to save over it?");

							}
						}
					}
					// if they want to save over, prompting for a help message.
					if (saveOver == 0) {
						if (testingLevel == false) {
							helpMessage = JOptionPane.showInputDialog(getParent(), "Set help message as: ");
							// ensuring the file can doesn't leave the help
							// message as nothing.
							if (helpMessage.equals("") || helpMessage.equals(" ")) {
								helpMessage = "null";

							}
							// swapping all the spaces for underscores so the
							// game doesn't get confused when reading it back
							// in.
							if (helpMessage != null) {
								for (int i = 0; i < helpMessage.length(); i++) {
									helpMessage = helpMessage.replaceAll(" ", "_");

								}
							}

						}
						// creating a tmp file for the level number file and
						// updating it.
						int levelNumber = 0;
						if (saveInput != null && saveOver == 0) {

							levelNumber = Integer.parseInt(saveInput);

							File levelNumberFile = new File(Config.pathForEditable("Levels/levelNumber.txt"));

							try {

								File tempFile = new File("myTempFile.txt");

								BufferedReader reader = new BufferedReader(new FileReader(levelNumberFile));
								BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
								boolean found = false;
								boolean updated = false;
								String lineToUpdate = saveInput + " ";

								String updatedLine = saveInput + " " + helpMessage + " " + (chapter);
								String currentLine;

								while ((currentLine = reader.readLine()) != null) {
									// updating the line that wants to be
									// changed
									if (currentLine.contains(lineToUpdate) && found == false) {

										writer.write(updatedLine + System.getProperty("line.separator"));
										found = true;

									} else {
										// writing the line that doesn't want to
										// be changed.
										writer.write(currentLine);
										writer.newLine();

									}

								}
								// writing the new line if the level doesn't
								// exist.
								if (found == false) {

									writer.write(updatedLine + System.getProperty("line.separator"));
								}

								writer.close();
								reader.close();
								boolean successful = tempFile.renameTo(levelNumberFile);
								// adding a new element to vecLevels
								if (testingLevel != true && vecLevels.contains("" + levelNumber) == false) {

									vecLevels.addElement("" + levelNumber);
									model = new DefaultComboBoxModel(vecLevels);
									levelsComboBox.setModel(model);

								}

							} catch (IOException e) {

							}

						}

						fileName = "Levels/level" + saveInput + ".txt";
						// saving the level to the file.
						try {
							PrintWriter outputStream = new PrintWriter(Config.pathForEditable(fileName));

							for (int x = 0; x < width; x++) {
								for (int y = 0; y < height; y++) {

									if (grid[x][y] != ("")) {
										outputStream.println((x + 30) + " " + (y + 20) + " " + grid[x][y]);
									}

								}

							}
							outputStream.close();
							loading = false;
							if (testingLevel == false) {
								JOptionPane.showMessageDialog(getParent(), "File saved as: " + fileName);
							}

						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			} catch (NumberFormatException e1) {
				// if the user attempts to enter an invalid save input, it does
				// not accept it.
				JOptionPane.showMessageDialog(getParent(), "Please enter an integer.");
				levelSaver();
			}
		}
	}

	public void painting() {
		// ensuring that the users mouse is not outside of the grid when
		// attempting to draw a tile.
		PointerInfo a = MouseInfo.getPointerInfo();
		Point b = a.getLocation();
		int mouseX = (int) b.getX();
		int mouseY = (int) b.getY();
		if (mouseX >= gridRect.getMinX() && mouseX <= gridRect.getMaxX() && mouseY >= gridRect.getMinY() + startY && mouseY <= (gridRect.getMaxY() + startY)) {
			drawTile();

		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		painting();

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		painting();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

		mouseDragged = false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
}
