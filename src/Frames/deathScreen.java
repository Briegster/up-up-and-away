package Frames;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Audio.BackgroundMusic;
import Config.Config;
import GameWorld.GameWorld;

@SuppressWarnings("serial")
public class deathScreen extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					// Initializing the death screen frame
					deathScreen frame = new deathScreen();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public deathScreen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 480, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		JButton btnLoadLastSave = new JButton("Restart level");
		btnLoadLastSave.setToolTipText("Restart level");
		btnLoadLastSave.setIcon(new ImageIcon(Config.pathForStatic("Images/PlayIcon.gif")));
		btnLoadLastSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// returning to the game.
				setFocusable(false);
				setVisible(false);

				GameWorld.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				GameWorld.window.getContentPane().removeAll();
				GameWorld.window.getContentPane().add(GameWorld.gameWorldFrame);
				GameWorld.window.validate();
				GameWorld.gameWorldFrame.repaint();
				GameWorld.gameWorldFrame.started = false;

				GameWorld.window.setSize(480, 500);
				GameWorld.window.setVisible(true);

				try {

					BackgroundMusic.changeMusic("bgm1");
					GameWorld.gameWorldFrame.init();

				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnLoadLastSave.setBounds(180, 190, 120, 32);
		contentPane.add(btnLoadLastSave);

		JButton btnMainMenu = new JButton("Main Menu");
		btnMainMenu.setIcon(new ImageIcon(Config.pathForStatic("Images/backIcon.gif")));
		btnMainMenu.setToolTipText("Return to main menu");
		btnMainMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// retuning to the menu
				setVisible(false);

				BackgroundMusic.changeMusic("bgm1");
				GameWorld.menuFrame.setVisible(true);

			}
		});
		btnMainMenu.setBounds(180, 245, 120, 32);
		contentPane.add(btnMainMenu);

		JButton btnExit = new JButton("Exit");
		btnExit.setToolTipText("Exit the game");
		btnExit.setIcon(new ImageIcon(Config.pathForStatic("Images/ExitIcon.gif")));
		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// quitting
				System.exit(0);
			}
		});
		btnExit.setBounds(180, 300, 120, 32);
		contentPane.add(btnExit);

		JLabel lblYouHaveDied = new JLabel("You Have Died");
		lblYouHaveDied.setIcon(new ImageIcon(Config.pathForStatic("Images/DeathScreen.jpg")));
		lblYouHaveDied.setBounds(0, 0, 480, 478);
		contentPane.add(lblYouHaveDied);
	}
}
