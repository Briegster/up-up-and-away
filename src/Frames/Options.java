package Frames;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import GameWorld.MenuFrame;

public class Options {

	public static OptionsFrame OptionsFrame = new OptionsFrame();
	public static MenuFrame MenuFrame = new MenuFrame();
	public static JFrame window = new JFrame("Options");

	public static void main(String[] args) {

		// TODO Auto-generated method stub

		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLayout(new BorderLayout());
		window.getContentPane().add(OptionsFrame);
		window.setSize(500, 500);
		window.setVisible(true);

	}
}
