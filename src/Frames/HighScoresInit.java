package Frames;

import javax.swing.JFrame;

public class HighScoresInit {
	// Initializing the high scores frame screen.
	public static HighScoresFrame highscoresFrame = new HighScoresFrame();

	public static JFrame window = new JFrame("Highscores");

	public static void main(String[] args) {

		// TODO Auto-generated method stub

		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLayout(null);
		window.getContentPane().add(highscoresFrame);
		window.setSize(500, 500);
		window.setVisible(true);

	}
}
