package Config;

import java.io.File;

public class Config {
	private static final String UUAA_EDITABLES = "UUAAEditables/";
	private final static String staticRoot = "/Users/joel.brieger/Documents/workspace/UpUpandAway/";
	private final static String editableRoot = System.getProperty("user.home");

	// Gets a path to a static resource file.
	// E.g. pathForStatic("Images/brick.gif") returns a path to the
	// resources/Images/brick.gif
	public static String pathForStatic(String p) {
		if (p.split("/").length <= 1) {
			System.out.println("Error: missing a subfolder for resource " + p);
		}
		return staticRoot + "src/Resources/" + p;
	}

	// Gets a path for an editable level or profile
	public static String pathForEditable(String p) {
		String root = editableRoot;
		if (!root.endsWith("/")) {
			root += "/";
		}
		return root + UUAA_EDITABLES + p;
	}

	// Makes the Profiles and Levels folder in the user's editable folder (if it
	// doesn't already exist)
	public static void makeEditableDirs() {
		String[] paths = { pathForEditable("Profiles"), pathForEditable("Levels") };
		for (String p : paths) {
			File f = new File(p);
			f.mkdirs();
		}
	}
}
