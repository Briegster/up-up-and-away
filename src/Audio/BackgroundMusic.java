package Audio;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import Config.Config;
import Frames.OptionsFrame;

public class BackgroundMusic {

	public static Clip clip;
	static String tmpMusic;
	public static BackgroundMusic currentMusic = new BackgroundMusic("");

	public BackgroundMusic(String fileName) {
		// creating the audio file

		File file = new File(fileName);
		try {
			// creating the input stream
			AudioInputStream ais = AudioSystem.getAudioInputStream(file);
			// getting and opening the clip and checking the volume.
			clip = AudioSystem.getClip();

			clip.open(ais);
			OptionsFrame.getVolume();

		} catch (Exception e) {

		}

	}

	public static void changeMusic(String currentString) {
		if (tmpMusic != currentString) {
			// stopping the music.
			if (currentMusic != null) {

				currentMusic.stop();
			}
			// changing the song based on the variable passed in.
			currentMusic = (new BackgroundMusic(Config.pathForStatic("Audio/" + currentString + ".wav")));

			currentMusic.loop();
		}

		tmpMusic = currentString;

	}

	public static void play() {
		// playing the clip
		try {

			if (clip != null) {

				new Thread() {

					@Override
					public void run() {

						synchronized (clip) {

							clip.stop();

							clip.setFramePosition(0);

							clip.start();

						}

					}

				}.start();

			}

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	public void stop() {
		// stopping the clip

		if (clip == null)
			return;

		clip.stop();

	}

	public void loop() {
		// looping the clip

		try {

			if (clip != null) {

				new Thread() {

					@Override
					public void run() {

						synchronized (clip) {

							clip.stop();

							clip.setFramePosition(0);

							clip.loop(Clip.LOOP_CONTINUOUSLY);

						}

					}

				}.start();

			}

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

}