package Audio;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import Config.Config;
import Frames.OptionsFrame;

public class SFX {
	public static Clip clip;

	public static Clip play(String filename, boolean autostart, double d) throws Exception {
		// checking if the sfx volume is not 0
		if (OptionsFrame.SFXVol != 0.0) {
			// creating a new audio input
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filename));
			clip = AudioSystem.getClip();
			// running the clip at the set volume
			try {
				clip.open(audioInputStream);
				clip.setFramePosition(0);
			} catch (LineUnavailableException e) {

			}

			OptionsFrame.getVolume();

			if (autostart)
				clip.start();
			return clip;
		}
		return clip;
	}

	// **************************************
	public void Clip(String[] args) throws Exception {
		Map<String, String> params = parseParams(args);
		// playing multiple sounds at once
		SFX test1 = new SFX();

		Clip clip1 = test1.play(params.get("clip1"), true, -5.0f);
		Clip clip2 = test1.play(params.get("clip2"), false, 5.0f);

		final long duration = 20000;
		final int interval = 500;
		for (long ts = 0; ts < duration; ts += interval) {

			if (ts > 3000 && !clip2.isRunning()) {
				clip2.setFramePosition(0);
				clip2.start();
			}
			if (!clip1.isRunning()) {
				clip1.close();
			}
			Thread.sleep(interval);
		}
	}

	private static Map<String, String> parseParams(String[] args) {
		Map<String, String> params = new HashMap<String, String>();
		for (String arg : args) {
			int delim = arg.indexOf('=');
			if (delim < 0)
				params.put("", arg.trim());
			else if (delim == 0)
				params.put("", arg.substring(1).trim());
			else
				params.put(arg.substring(0, delim).trim(), arg.substring(delim + 1).trim());
		}
		return params;
	}

	public static void rndSound(int rndlim, String sound) {
		// if there a multiple sounds for one situation, it generates a random
		// number and plays the sound, depending on that number.
		Random rnd = new Random();
		int num = rnd.nextInt(rndlim) + 1;

		try {
			SFX.play(Config.pathForStatic("Audio/" + sound + num + ".wav"), true, 0.5);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	public static void itemSound(String item) {
		// if there is only one sound, it plays it.
		try {
			SFX.play(Config.pathForStatic("Audio/" + item + ".wav"), true, 0.5);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

}
