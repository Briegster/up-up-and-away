package Entities;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import Audio.BackgroundMusic;
import Audio.SFX;
import Frames.Editor;
import GameWorld.GameWorld;
import GameWorld.GameWorldFrame;
import GameWorld.Gate;
import GameWorld.LevelGrid;
import Items.JumpItemClass;
import Items.ShieldItemClass;

public class Player extends KeyAdapter {
	public int height;
	public int width;
	public int cornerx;
	public int cornery;
	public String name;
	public String onTop;

	int killRightX = 1560;
	int killLeftX = 40;
	int killDownY = 960;
	int pixels = 16;
	public static int cheat1 = 0;
	public static int cheat2 = 0;

	int speed;
	int jumpTime = 0;
	public int maxJump;
	public int health;
	int maxHealth;
	public static int tempJumpHeight;
	public static int keyCount = 0;
	boolean onAndy = false;

	public static int cloudmovespeed = 30;
	public static int cloudaccell = 3;

	public static boolean keya = false;
	public static boolean keys = false;
	public static boolean keyd = false;
	public static boolean keyw = false;
	public static boolean keye = false;
	public static boolean keySpace = false;

	public boolean jumping = false;
	public boolean grounded = true;
	public boolean hitting = false;
	public boolean atDoor = false;
	public static int currentPlayerToken;

	public int playerDirection = 1;
	LevelGrid level;

	public static ArrayList<MovingEnemy> arrMovingSpike;
	public static ArrayList<Spike> arrSpike;

	ImageIcon playerImage;

	@SuppressWarnings("static-access")
	public Player(String name, ImageIcon playerImage, LevelGrid level, ArrayList<MovingEnemy> arrMovingSpike, ArrayList<Spike> arrSpike, int cornerx,
			int cornery, int speed, int height, int width, int maxjump, int maxHealth, String onTop, boolean jumping, boolean atDoor, boolean onAndy,
			int jumpTime) {
		// initialising all of the variables.
		this.name = name;
		this.level = level;
		this.arrMovingSpike = arrMovingSpike;
		this.arrSpike = arrSpike;
		this.cornerx = cornerx;
		this.cornery = cornery;
		this.height = height;
		this.width = width;
		this.speed = speed;
		this.maxJump = maxjump;
		this.health = maxHealth;
		this.maxHealth = maxHealth;
		this.playerImage = playerImage;
		this.onTop = onTop;
		this.jumping = jumping;
		this.atDoor = atDoor;
		this.onAndy = onAndy;
		this.jumpTime = jumpTime;

	}

	public Image getPlayerImg(ImageIcon playerImage) {

		return playerImage.getImage();
	}

	public void draw(Graphics g, int x, int y) {

		g.drawImage(playerImage.getImage(), cornerx + x, cornery + y, null);

	}

	public void update() {
		// checking for the jump cases
		jumpCases();
		// running gravity
		gravity();
		// running jump
		jump();
		// this if statement ensures that the keys don't movie characters that
		// aren't active.
		if (GameWorld.gameWorldFrame.arrCharacters.get(currentPlayerToken) == this) {
			// moving the character left, the clouds, and the person whos ontop
			// of the character.
			if (keya == true) {

				cornerx -= speed;

				for (int p = 0; p < GameWorld.gameWorldFrame.arrCharacters.size(); p++) {

					if (GameWorld.gameWorldFrame.arrCharacters.get(p) != this
							&& GameWorld.gameWorldFrame.arrCharacters.get(p).onTop.equals(GameWorld.gameWorldFrame.arrCharacters.get(p).name + "/" + name)) {

						GameWorld.gameWorldFrame.arrCharacters.get(p).cornerx -= speed;

					}
				}
				cloudmovespeed -= cloudaccell;

				moveLeft();
			}

			if (keyd == true) {
				cornerx += speed;

				for (int p = 0; p < GameWorld.gameWorldFrame.arrCharacters.size(); p++) {

					if (GameWorld.gameWorldFrame.arrCharacters.get(p).onTop.equals(GameWorld.gameWorldFrame.arrCharacters.get(p).name + "/" + name)
							&& GameWorld.gameWorldFrame.arrCharacters.get(p) != this) {
						GameWorld.gameWorldFrame.arrCharacters.get(p).cornerx += speed;

					}

				}
				cloudmovespeed += cloudaccell;

				moveRight();
			}
			// jumping the character.
			if (keyw == true) {

				if (grounded == true) {
					jumping = true;
					SFX.rndSound(3, "jump");
					grounded = false;
				}

			}
			// moving down.
			if (keys == true) {
				cornery += speed;
				jumping = false;
				moveDown();

			}
		}
		// checking if the character runs out of health, or leaves the map.
		if (health <= 0 || cornery >= killDownY || cornerx <= killLeftX || cornerx >= killRightX) {
			die();
		}
		// running all of the interact methods, which checks if the character is
		// interacting with them.
		movingSpikeInteract();
		spikeInteract();
		healthItemInteract();
		jumpBoostItemInteract();
		shieldItemInteract();
		Gate.gateInteract();
		keyInteract();
		coinInteract();
		waterInteract();

	}

	@SuppressWarnings("static-access")
	@Override
	public void keyPressed(KeyEvent e) {
		// checking if the user is pressing the buttons that they have set in
		// the options menu.
		if (e.getKeyChar() == Frames.OptionsFrame.leftChar) {
			keya = true;
			cheat1 += 1;

			playerDirection = -1;

		}
		if (e.getKeyChar() == Frames.OptionsFrame.downChar) {
			cheat2 += 1;

			keys = true;

		}
		if (e.getKeyChar() == Frames.OptionsFrame.jumpChar) {

			keyw = true;

		}
		if (e.getKeyChar() == Frames.OptionsFrame.rightChar) {
			keyd = true;
			playerDirection = 1;

			GameWorld.gameWorldFrame.cheat3 += 1;

		}
		if (e.getKeyChar() == 'k') {
			Player.cheat1 = 0;
			Player.cheat2 = 0;
			GameWorld.gameWorldFrame.cheat3 = 0;

		}
		if (e.getKeyChar() == Frames.OptionsFrame.openChar) {

			keye = true;
		}

		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {

			GameWorld.gameWorldFrame.mainTimer.stop();
			GameWorld.window.setVisible(false);
			if (Frames.EditorFrame.testingLevel == false) {

				GameWorld.menuFrame.btnResume.setVisible(true);
				GameWorld.menuFrame.lblProfileName.setText("Profile: " + GameWorld.menuFrame.profileName + " | Level: " + LevelGrid.levelNum + " | Score: "
						+ GameWorldFrame.score);
				GameWorld.menuFrame.setVisible(true);

			} else if (Frames.EditorFrame.testingLevel == true) {
				Frames.EditorFrame.testingLevel = false;
				Editor.window.setVisible(true);
			}

		}
		if (e.getKeyChar() == Frames.OptionsFrame.attackChar) {

			keySpace = true;
			SFX.rndSound(4, "shot");
			doattack();

		}
		if (e.getKeyChar() == Frames.OptionsFrame.nextCharacChar) {

			currentPlayerToken = (currentPlayerToken += 1) % GameWorld.gameWorldFrame.arrCharacters.size();
			switchCharacter();

		}
		if (e.getKeyChar() == Frames.OptionsFrame.Charac1Char && GameWorld.gameWorldFrame.arrCharacters.size() >= 1) {

			currentPlayerToken = 0;
			switchCharacter();

		}
		if (e.getKeyChar() == Frames.OptionsFrame.Charac2Char && GameWorld.gameWorldFrame.arrCharacters.size() >= 2) {

			currentPlayerToken = 1;
			switchCharacter();

		}
		if (e.getKeyChar() == Frames.OptionsFrame.Charac3Char) {
			if (GameWorld.gameWorldFrame.arrCharacters.size() >= 3) {
				currentPlayerToken = 2;
				switchCharacter();
			}

		}
		if (e.getKeyChar() == Frames.OptionsFrame.Charac4Char && GameWorld.gameWorldFrame.arrCharacters.size() == 4) {

			currentPlayerToken = 3;
			switchCharacter();

		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

		if (e.getKeyChar() == Frames.OptionsFrame.leftChar) {
			keya = false;

		}
		if (e.getKeyChar() == Frames.OptionsFrame.downChar) {
			keys = false;

		}
		if (e.getKeyChar() == Frames.OptionsFrame.jumpChar) {
			keyw = false;

		}
		if (e.getKeyChar() == Frames.OptionsFrame.rightChar) {
			keyd = false;

		}
		if (e.getKeyChar() == Frames.OptionsFrame.openChar) {
			keye = false;
		}

	}

	public void jumpCases() {
		// These are the three different situations in which jumping is handled.

		for (int i = 0; i < GameWorld.gameWorldFrame.arrCharacters.size(); i++) {
			Player target = GameWorld.gameWorldFrame.arrCharacters.get(i);
			// The first case is testing if a character is bouncing on Andy
			if (target.onTop.equals(target.name + "/Andy") && name != "Andy") {

				target.jumping = true;
				SFX.rndSound(3, "jump");
				target.onAndy = true;
				target.onTop = ("");

			}

			if (target.jumping == true && target.onAndy == true && JumpItemClass.boostBoolean == false) {
				target.jumpTime++;
				if (target.jumpTime >= (target.maxJump + 5) * GameWorld.gameWorldFrame.arrCharacters.size()) {
					target.jumpTime = 0;

					if (target.jumping == true) {
						target.onAndy = false;
						target.jumping = false;

					}

				}
				// The second case is if they are simply jumping.
			} else if (target.jumping == true && target.onAndy == false && JumpItemClass.boostBoolean == false) {

				target.jumpTime++;
				if (target.jumpTime >= target.maxJump * GameWorld.gameWorldFrame.arrCharacters.size()) {
					target.jumpTime = 0;

					if (target.jumping == true) {

						target.jumping = false;

					}

				}
				// The third case is if they are jumping whilst a jump boost has
				// been activated.
			} else if (target.jumping == true && JumpItemClass.boostBoolean == true) {

				target.jumpTime++;
				if (target.jumpTime >= (target.maxJump + 5) * GameWorld.gameWorldFrame.arrCharacters.size()) {
					target.jumpTime = 0;

					if (target.jumping == true) {

						target.jumping = false;

					}

				}
			}
		}

	}

	public void doattack() {
		// Shooting, the player loses health, and the health label is reset.
		GameWorld.gameWorldFrame.currentPlayer.health -= 1;

		GameWorldFrame.lblHealth.setText(Integer.toString(GameWorld.gameWorldFrame.currentPlayer.health));
		// Determining if the bullet should move to the left or right.
		if (playerDirection == -1) {
			GameWorld.gameWorldFrame.arrAttack.add(new Attack(-1, 10, GameWorld.gameWorldFrame.currentPlayer.cornerx,
					GameWorld.gameWorldFrame.currentPlayer.cornery));

		} else if (playerDirection == 1) {
			GameWorld.gameWorldFrame.arrAttack.add(new Attack(1, 10, GameWorld.gameWorldFrame.currentPlayer.cornerx,
					GameWorld.gameWorldFrame.currentPlayer.cornery));

		}

	}

	public void switchCharacter() {
		// Setting the current player to the selected character.
		GameWorld.gameWorldFrame.currentPlayer = GameWorld.gameWorldFrame.arrCharacters.get(currentPlayerToken);
		tempJumpHeight = GameWorld.gameWorldFrame.currentPlayer.maxJump;
		GameWorldFrame.lblHealth.setText(Integer.toString(GameWorld.gameWorldFrame.arrCharacters.get(currentPlayerToken).health));
	}

	public void takeDamage() {
		// Taking damage.
		if (hitting && ShieldItemClass.shieldBoolean == false) {

			health -= 1;
			SFX.rndSound(6, "hit");

			if (GameWorld.gameWorldFrame.arrCharacters.get(currentPlayerToken) == this) {
				GameWorldFrame.lblHealth.setText(Integer.toString(health));
			}

		}
	}

	public void die() {
		// Dying, stopping the game, changing to the death screen.
		GameWorld.gameWorldFrame.died = true;
		LevelGrid.erase();
		GameWorld.gameWorldFrame.mainTimer.stop();
		GameWorld.window.dispose();
		BackgroundMusic.currentMusic.stop();
		BackgroundMusic.changeMusic("GameOver");
		GameWorld.deathScreen.setVisible(true);

	}

	// These are specific mathematical values for the characters co-ordinates in
	// the grid.
	public int pixelsToX() {
		return (int) Math.floor(cornerx / 16.0);
	}

	public int pixelsToXRight() {
		return (int) Math.ceil(cornerx / 16.0) - 1;
	}

	public int pixelsToY() {
		return (int) Math.floor(cornery / 16.0);
	}

	// Setting the players bounds.
	public Rectangle getPlayerBounds() {
		return new Rectangle(cornerx, cornery, width * 16, height * 16);

	}

	public Rectangle getPlayerUpBounds() {
		return new Rectangle(cornerx, cornery - 1, width * 16, height * 16);

	}

	// Determining if the player is touching another player.
	public boolean touchingOtherPlayer() {
		for (int i = 0; i < GameWorld.gameWorldFrame.arrCharacters.size(); i++) {
			Player target = GameWorld.gameWorldFrame.arrCharacters.get(i);
			if (target != this) {

				if (getPlayerBounds().intersects(target.getPlayerBounds())) {
					return true;
				}
			}
		}
		return false;
	}

	// Checking if they are touchinga gate.
	public boolean touchingGate() {
		for (int i = 0; i < GameWorld.gameWorldFrame.arrGate.size(); i++) {
			Gate target = GameWorld.gameWorldFrame.arrGate.get(i);

			if (getPlayerBounds().intersects(target.getGateBounds()) && target.opened == false) {
				target.touching = true;
				return true;
			} else {
				target.touching = false;
			}

		}
		return false;

	}

	public void jump() {
		// This determines whether the jumping player has hit a wall, and if so,
		// pushes them back.
		if (jumping == true) {

			cornery -= speed;

			int rightx = (int) Math.ceil(new Double(cornerx + width * pixels) / pixels) - 1;

			for (int ix = pixelsToX(); ix <= rightx; ix++) {

				if (level.tiles[ix][pixelsToY()].equals("platform")) {

					cornery += speed;
					jumpTime = 0;
					jumping = false;

				}
			}

		}
		if (touchingGate()) {

			cornery += speed;
			jumpTime = 0;
			jumping = false;

		}

		if (touchingOtherPlayer()) {
			for (int i = 0; i < GameWorld.gameWorldFrame.arrCharacters.size(); i++) {
				Player target = GameWorld.gameWorldFrame.arrCharacters.get(i);
				if (target != this && jumping == true) {

					cornery += speed;
					jumpTime = 0;
					jumping = false;
					return;

				}

			}
		}
	}

	@SuppressWarnings("static-access")
	public void gravity() {
		// If the character is not jumping, then they are falling, and this
		// determines if they have hit a wall bellow

		if (jumping == false) {

			cornery += speed;

			for (int i = 0; i < GameWorld.gameWorldFrame.arrCharacters.size(); i++) {
				Player target = GameWorld.gameWorldFrame.arrCharacters.get(i);

				if (this != target) {
					// This particular algorithm checks if they are above
					// another character.
					if ((getPlayerBounds().intersects(target.getPlayerUpBounds())) && touchingOtherPlayer()) {

						cornery -= speed;
						jumping = false;
						grounded = true;
						onTop = name + "/" + target.name;
						return;

					} else {
						onTop = "";

					}

				}

			}
			int bottomy = height + (int) Math.ceil(new Double(cornery) / pixels) - 1;
			int rightx = (int) Math.ceil(new Double(cornerx + width * pixels) / pixels) - 1;
			for (int ix = pixelsToX(); ix <= rightx; ix++) {

				if (level.tiles[ix][bottomy].equals("platform")) {
					cornery -= speed;
					grounded = true;
					return;
				}
			}
			if (touchingGate()) {
				cornery -= speed;
				jumping = false;
				grounded = true;
				return;

			}

		}

	}

	@SuppressWarnings("static-access")
	public void moveDown() {
		// Making sure the user can't smash through the ground.
		int bottomy = height + (int) Math.ceil(new Double(cornery) / pixels) - 1;
		for (int ix = 0; ix <= width; ix++) {

			if (level.tiles[pixelsToX() + ix][bottomy].equals("platform")) {
				cornery -= speed;
				jumpTime = 0;
				jumping = false;
				grounded = true;
				return;

			}
		}
		if (touchingGate()) {
			cornery -= speed;
			jumpTime = 0;
			jumping = false;
			grounded = true;
			return;

		}
		if (touchingOtherPlayer()) {
			cornery -= speed;
			jumpTime = 0;
			jumping = false;
			grounded = true;
			return;

		}

	}

	@SuppressWarnings("static-access")
	public void moveLeft() {
		// Checking if the user is hitting a wall to their left.
		int bottomy = (int) Math.ceil(new Double(cornery + height * pixels) / pixels) - 1;

		for (int iy = pixelsToY(); iy <= bottomy; iy++) {

			if (level.tiles[pixelsToX()][iy].equals("platform")) {
				cornerx += speed;
				cloudmovespeed += cloudaccell;
				// stopping a character who is on top of another.
				for (int p = 0; p < GameWorld.gameWorldFrame.arrCharacters.size(); p++) {
					Player t = GameWorld.gameWorldFrame.arrCharacters.get(p);
					if (t.onTop.equals(t.name + "/" + name) && t != this) {
						t.cornerx += speed;

					}
				}
				return;
			}

		}
		// Checking if the user is on top of another, and whilst they are on
		// top,
		// they hit a wall, when the person below does not.

		for (int p = 0; p < GameWorld.gameWorldFrame.arrCharacters.size(); p++) {
			Player t = GameWorld.gameWorldFrame.arrCharacters.get(p);
			if (t.onTop.equals(t.name + "/" + name) && t != this) {
				for (int iy = (int) Math.floor(t.cornery / 16.0); iy <= (int) Math.ceil(new Double(t.cornery + t.height * 16) / 16) - 1; iy++) {
					if (level.tiles[t.cornerx / 16][iy].equals("platform")) {
						t.cornerx += speed;

					}

				}
				// stopping them if they hit someone, whislt on top of someone
				// else.
				if (t.touchingOtherPlayer()) {
					t.cornerx += speed;
				}

			}

		}

		if (touchingGate()) {
			cornerx += speed;
			cloudmovespeed += cloudaccell;
			return;

		}
		if (touchingOtherPlayer()) {
			cornerx += speed;
			cloudmovespeed += cloudaccell;
			for (int p = 0; p < GameWorld.gameWorldFrame.arrCharacters.size(); p++) {
				Player t = GameWorld.gameWorldFrame.arrCharacters.get(p);
				if (t.onTop.equals(t.name + "/" + name) && t != this) {
					t.cornerx += speed;
				}

			}
			return;
		}

	}

	@SuppressWarnings("static-access")
	public void moveRight() {

		// Loop through every tile along your height,
		// Check if your rightmost edge at that height is inside a tile
		// If so, push the player back and return.
		int bottomy = (int) Math.ceil(new Double(cornery + height * 16) / 16) - 1;

		for (int iy = pixelsToY(); iy <= bottomy; iy++) {
			if (level.tiles[pixelsToXRight() + width][iy].equals("platform")) {
				cornerx -= speed;
				cloudmovespeed -= cloudaccell;

				for (int p = 0; p < GameWorld.gameWorldFrame.arrCharacters.size(); p++) {
					Player t = GameWorld.gameWorldFrame.arrCharacters.get(p);
					if (t.onTop.equals(t.name + "/" + name) && t != this) {
						t.cornerx -= speed;

					}
				}
				return;
			}

		}

		// this is to knock them off if they hit a wall while on top of you
		for (int p = 0; p < GameWorld.gameWorldFrame.arrCharacters.size(); p++) {
			Player t = GameWorld.gameWorldFrame.arrCharacters.get(p);
			if (t.onTop.equals(t.name + "/" + name) && t != this) {
				for (int iy = (int) Math.floor(t.cornery / 16.0); iy <= (int) Math.ceil(new Double(t.cornery + t.height * 16) / 16) - 1; iy++) {
					if (level.tiles[t.pixelsToXRight() + t.width][iy].equals("platform")) {
						t.cornerx -= speed;
					}
				}

				if (t.touchingOtherPlayer()) {
					t.cornerx -= speed;
				}

			}

		}
		// this is making gates unpassable
		if (touchingGate()) {
			cornerx -= speed;
			cloudmovespeed -= cloudaccell;
			return;
		}
		// this makes other players unpassable
		if (touchingOtherPlayer()) {
			cornerx -= speed;
			cloudmovespeed -= cloudaccell;
			for (int p = 0; p < GameWorld.gameWorldFrame.arrCharacters.size(); p++) {
				Player t = GameWorld.gameWorldFrame.arrCharacters.get(p);
				if (t.onTop.equals(t.name + "/" + name) && t != this) {
					t.cornerx -= speed;
				}

			}
			return;
		}

	}

	@SuppressWarnings("static-access")
	public void movingSpikeInteract() {

		MovingEnemy.deathTimer();
		for (int m = 0; m < arrMovingSpike.size(); m++) {
			// Checking if the player is attempting to smash a spike.
			if (pixelsToX() <= arrMovingSpike.get(m).cornerx / pixels && arrMovingSpike.get(m).cornerx / pixels <= pixelsToX() + width
					&& pixelsToY() + height == arrMovingSpike.get(m).cornery / pixels && Player.keys == true) {

				GameWorld.gameWorldFrame.arrMovingSpike.remove(m);
				SFX.itemSound("kill");
				GameWorld.gameWorldFrame.score++;
				GameWorldFrame.lblScore.setText(Integer.toString(GameWorld.gameWorldFrame.score));
			} else if (getPlayerBounds().intersects(GameWorld.gameWorldFrame.arrMovingSpike.get(m).getMovingSpikeBounds())
					&& GameWorld.gameWorldFrame.arrMovingSpike.get(m).movingSpikeIcon != Spike.spikeDeathIcon) {

				hitting = true;

				takeDamage();

			} else {
				hitting = false;

			}
		}

		try {
			// checking if the player has killed a spike, and play its death
			// animation.
			for (int m = 0; m < Player.arrMovingSpike.size(); m++) {
				if (GameWorld.gameWorldFrame.arrMovingSpike.get(m).startDeathCount) {
					GameWorld.gameWorldFrame.arrMovingSpike.get(m).deathCount++;
				}
				if (GameWorld.gameWorldFrame.arrMovingSpike.get(m).deathCount >= 10 * arrMovingSpike.size()
						&& GameWorld.gameWorldFrame.arrMovingSpike.get(m).movingSpikeIcon == MovingEnemy.spikeDeathIcon) {

					GameWorld.gameWorldFrame.arrMovingSpike.get(m).startDeathCount = false;
					GameWorld.gameWorldFrame.arrMovingSpike.remove(m);

				}
				// Checking if the bullet hit the spike, and if so, starting the
				// death count to play the animation.
				for (int b = 0; b < GameWorld.gameWorldFrame.arrAttack.size(); b++) {

					if (GameWorld.gameWorldFrame.arrMovingSpike.get(m).getMovingSpikeBounds()
							.intersects(GameWorld.gameWorldFrame.arrAttack.get(b).getAttackBounds())) {

						GameWorld.gameWorldFrame.arrMovingSpike.get(m).movingSpikeIcon = MovingEnemy.spikeDeathIcon;
						SFX.itemSound("kill");
						GameWorld.gameWorldFrame.arrMovingSpike.get(m).startDeathCount = true;

						GameWorld.gameWorldFrame.arrAttack.remove(b);
						GameWorld.gameWorldFrame.score++;
						GameWorldFrame.lblScore.setText(Integer.toString(GameWorld.gameWorldFrame.score));
					}

				}

			}
		} catch (Exception e) {

		}

	}

	public void spikeInteract() {
		Spike.deathTimer();
		for (int i = 0; i < GameWorld.gameWorldFrame.arrSpike.size(); i++) {

			// smashing into a spike
			if (pixelsToX() <= arrSpike.get(i).cornerx / pixels && arrSpike.get(i).cornerx / pixels <= pixelsToX() + width
					&& pixelsToY() + height == arrSpike.get(i).cornery / pixels && Player.keys == true) {

				GameWorld.gameWorldFrame.arrSpike.get(i).spikeIcon = Spike.spikeDeathIcon;
				SFX.itemSound("kill");
				Spike.startDeathCount = true;
				GameWorld.gameWorldFrame.score++;
				GameWorldFrame.lblScore.setText(Integer.toString(GameWorld.gameWorldFrame.score));
				// being hit by a spike
			} else if (getPlayerBounds().intersects(GameWorld.gameWorldFrame.arrSpike.get(i).getSpikeBounds())
					&& GameWorld.gameWorldFrame.arrSpike.get(i).spikeIcon != Spike.spikeDeathIcon) {

				hitting = true;
				takeDamage();

			} else {
				hitting = false;

			}
		}

		try {
			// shooting the spike
			for (int b = 0; b < GameWorld.gameWorldFrame.arrAttack.size(); b++) {
				for (int m = 0; m < arrSpike.size(); m++)
					if (GameWorld.gameWorldFrame.arrSpike.get(m).getSpikeBounds().intersects(GameWorld.gameWorldFrame.arrAttack.get(b).getAttackBounds())) {

						GameWorld.gameWorldFrame.arrSpike.get(m).spikeIcon = Spike.spikeDeathIcon;
						Spike.startDeathCount = true;
						GameWorld.gameWorldFrame.arrAttack.remove(b);
						SFX.itemSound("kill");
						GameWorld.gameWorldFrame.score++;
						GameWorldFrame.lblScore.setText(Integer.toString(GameWorld.gameWorldFrame.score));
					}

			}

		} catch (IndexOutOfBoundsException e) {

		}
	}

	public void coinInteract() {
		// interacting with the coin, adds to the score and removes the coin.
		for (int i = 0; i < GameWorld.gameWorldFrame.arrCoin.size(); i++) {

			if (getPlayerBounds().intersects(GameWorld.gameWorldFrame.arrCoin.get(i).getCoinBounds())) {

				GameWorld.gameWorldFrame.arrCoin.remove(i);
				SFX.itemSound("coin");
				GameWorld.gameWorldFrame.score++;
				GameWorldFrame.lblScore.setText(Integer.toString(GameWorld.gameWorldFrame.score));

			}
		}
	}

	public void healthItemInteract() {
		// gives 20 health back, but doesn't go over max health.
		for (int i = 0; i < GameWorld.gameWorldFrame.arrHealthItem.size(); i++) {
			if (getPlayerBounds().intersects(GameWorld.gameWorldFrame.arrHealthItem.get(i).getHealthBounds())) {

				health += 20;
				if (health >= maxHealth) {
					health = maxHealth;
				}
				SFX.itemSound("health");
				GameWorld.gameWorldFrame.arrHealthItem.remove(i);
				GameWorldFrame.lblHealth.setText(Integer.toString(health));

			}
		}
	}

	public void jumpBoostItemInteract() {
		// sets the boost boolean to true, which alters the case that occurs in
		// the jumpCases method.
		for (int i = 0; i < GameWorld.gameWorldFrame.arrJumpBoostItem.size(); i++) {

			if (getPlayerBounds().intersects(GameWorld.gameWorldFrame.arrJumpBoostItem.get(i).getJumpBoostBounds())) {
				tempJumpHeight = maxJump;
				JumpItemClass.boostBoolean = true;

				JumpItemClass.boostTimer = 0;
				SFX.itemSound("jumpBoost");
				GameWorld.gameWorldFrame.arrJumpBoostItem.remove(i);

			}

		}
		// the boost timer, disables the ability after a certain amount of time.
		if (JumpItemClass.boostBoolean) {
			JumpItemClass.boostTimer++;

			if (JumpItemClass.boostTimer >= 1500) {
				JumpItemClass.boostBoolean = false;

				JumpItemClass.boostTimer = 0;

			}
		}

	}

	public void shieldItemInteract() {
		// sets the shield boolean to be true, which disables taking damage in
		// the take damage method.
		for (int i = 0; i < GameWorld.gameWorldFrame.arrShieldItem.size(); i++) {

			if (getPlayerBounds().intersects(GameWorld.gameWorldFrame.arrShieldItem.get(i).getShieldItemBounds())) {
				SFX.itemSound("Shield");
				ShieldItemClass.shieldBoolean = true;

				ShieldItemClass.shieldTimer = 0;

				GameWorld.gameWorldFrame.arrShieldItem.remove(i);

			}
		}
		// The shield timer, disables the ability after a certain amount of
		// time.
		if (ShieldItemClass.shieldBoolean) {
			ShieldItemClass.shieldTimer++;

			if (ShieldItemClass.shieldTimer >= 1500) {
				ShieldItemClass.shieldBoolean = false;

				ShieldItemClass.shieldTimer = 0;
			}
		}
	}

	public void keyInteract() {
		// Adds to the key counter, allowing the user to open a gate.
		for (int i = 0; i < GameWorld.gameWorldFrame.arrKey.size(); i++) {

			if (getPlayerBounds().intersects(GameWorld.gameWorldFrame.arrKey.get(i).getKeyBounds())) {
				Player.keyCount += 1;
				SFX.itemSound("key");
				GameWorldFrame.lblKeyCount.setText(Integer.toString(Player.keyCount));
				GameWorld.gameWorldFrame.arrKey.remove(i);

			}
		}
	}

	public void waterInteract() {
		// chekcing if the user interacts with water, and is not brick, (one of
		// bricks powers is immunity to water.)
		int bottomy = height + (int) Math.ceil(new Double(cornery) / pixels) - 1;
		int rightx = (int) Math.ceil(new Double(cornerx + width * pixels) / pixels) - 1;
		if (name != "Brick") {
			for (int ix = pixelsToX(); ix <= rightx; ix++) {
				for (int iy = pixelsToY(); iy <= bottomy; iy++) {

					if (level.tiles[ix][iy].equals("water") && ShieldItemClass.shieldBoolean == false) {
						health--;
						SFX.rndSound(6, "hit");
						GameWorldFrame.lblHealth.setText(Integer.toString(health));

					}
				}
			}
		}

	}

}
