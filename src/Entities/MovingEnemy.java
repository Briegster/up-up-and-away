package Entities;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;

import Config.Config;
import GameWorld.GameWorld;
import GameWorld.Gate;
import GameWorld.LevelGrid;

public class MovingEnemy implements ActionListener {
	int speed = 6;
	int direction = 1;

	int height = 1;
	int width = 1;
	int cornerx;
	int cornery;

	static boolean startDeathCount = false;
	static int deathCount;
	boolean hitting = false;
	ImageIcon movingSpikeIcon = new ImageIcon(Config.pathForStatic("Images/movingSpikeLeft.gif"));
	ImageIcon movingSpikeLeftIcon = new ImageIcon(Config.pathForStatic("Images/movingSpikeLeft.gif"));
	ImageIcon movingSpikeRightIcon = new ImageIcon(Config.pathForStatic("Images/movingSpikeRight.gif"));
	ImageIcon transparentIcon = new ImageIcon(Config.pathForStatic("Images/transparent.gif"));

	static ImageIcon spikeDeathIcon = new ImageIcon(Config.pathForStatic("Images/SpikeDeathAni.gif"));

	@SuppressWarnings("static-access")
	public MovingEnemy(int x, int y, LevelGrid currentLevel, ImageIcon movingSpikeIcon, int deathCount, boolean startDeathCount) {
		this.cornerx = x * 16;
		this.cornery = y * 16;
		this.deathCount = deathCount;
		this.startDeathCount = startDeathCount;

	}

	@SuppressWarnings("static-access")
	public static void deathTimer() {

	}

	public Rectangle getMovingSpikeBounds() {
		return new Rectangle(cornerx, cornery, 16, 16);

	}

	public void move() {
		// checking the direction of the moving spike
		if (direction < 0 && movingSpikeIcon != spikeDeathIcon) {
			this.cornerx += speed * direction;
			platformEnd();
			movingSpikeIcon = movingSpikeLeftIcon;
			moveLeft();
		} else if (direction > 0 && movingSpikeIcon != spikeDeathIcon) {
			this.cornerx += speed * direction;
			platformEnd();
			movingSpikeIcon = movingSpikeRightIcon;
			moveRight();
		}

	}

	@SuppressWarnings("static-access")
	public void platformEnd() {
		// seeing if the spike needs to change direction
		cornery += speed;
		boolean falling = false;

		int bottomy = height + (int) Math.ceil(new Double(cornery) / 16) - 1;
		for (int ix = 0; ix < width; ix++) {
			if (GameWorld.gameWorldFrame.currentLevel.tiles[cornerx / 16 + ix][bottomy].equals("platform")) {
				cornery -= speed;
				falling = true;
			}
		}
		if (falling == false && GameWorld.gameWorldFrame.currentLevel.tiles[cornerx / 16][bottomy].equals("platform")) {
			cornery -= speed;
			falling = true;
		}

		if (falling == false) {
			direction *= -1;
			this.cornerx += speed * direction;
			cornery -= speed;
		}

	}

	@SuppressWarnings("static-access")
	public void moveLeft() {
		// changing direction when moving left
		int bottomy = (int) Math.ceil(new Double(cornery + height * 16) / 16) - 1;
		for (int iy = cornery / 16; iy <= bottomy; iy++) {
			// checking if the grid we are about to move into is unpassable
			if (GameWorld.gameWorldFrame.currentLevel.tiles[cornerx / 16][iy].equals("platform")) {
				direction *= -1;

				return;
			}
		}

		for (int i = 0; i < GameWorld.gameWorldFrame.arrGate.size(); i++) {
			Gate target = GameWorld.gameWorldFrame.arrGate.get(i);
			if (getMovingSpikeBounds().intersects(target.getGateBounds())) {
				direction *= -1;

				return;
			}
		}

	}

	@SuppressWarnings("static-access")
	public void moveRight() {
		// changing direction when moving right
		int bottomy = (int) Math.ceil(new Double(cornery + height * 16) / 16) - 1;
		for (int iy = cornery / 16; iy <= bottomy; iy++) {
			if (GameWorld.gameWorldFrame.currentLevel.tiles[cornerx / 16 + width][iy].equals("platform")) {
				direction *= -1;

				return;
			}
		}

		for (int i = 0; i < GameWorld.gameWorldFrame.arrGate.size(); i++) {
			Gate target = GameWorld.gameWorldFrame.arrGate.get(i);
			if (getMovingSpikeBounds().intersects(target.getGateBounds())) {
				direction *= -1;

				return;
			}
		}
	}

	public void draw(Graphics2D p, int startx, int starty) {

		p.drawImage(movingSpikeIcon.getImage(), cornerx + startx, cornery + starty, null);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
