package Entities;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import Config.Config;
import GameWorld.GameWorld;
import GameWorld.LevelGrid;

public class Attack {

	public static int speed = 15;
	int cornerx;
	int cornery;
	int direction;
	boolean cooldown = false;
	int attackSize;
	ImageIcon rockIcon = new ImageIcon(Config.pathForStatic("Images/Rock.gif"));

	public Attack(int direction, int attackSize, int x, int y) {

		this.cornerx = x;
		this.cornery = y + 12;
		this.direction = direction;
		this.attackSize = attackSize;

	}

	public void move() {
		// determining the direction the newly created bullet will move

		for (int i = 0; i < 1; i++) {

			if (GameWorld.gameWorldFrame.arrAttack.get(i).direction == -1) {
				this.cornerx += speed * direction;

			} else if (GameWorld.gameWorldFrame.arrAttack.get(i).direction == 1) {
				this.cornerx += speed * direction;

			}

		}
		// checking if the bullet hits a wall.
		for (int a = 0; a < GameWorld.gameWorldFrame.arrAttack.size(); a++) {
			if (GameWorld.gameWorldFrame.arrAttack.get(a).cornerx / 16 >= LevelGrid.width
					|| GameWorld.gameWorldFrame.arrAttack.get(a).cornerx / 16 <= 0
					|| GameWorld.gameWorldFrame.currentLevel.tiles[GameWorld.gameWorldFrame.arrAttack.get(a).cornerx / 16][GameWorld.gameWorldFrame.arrAttack
							.get(a).cornery / 16].equals("platform")) {

				GameWorld.gameWorldFrame.arrAttack.remove(a);
			}
		}
	}

	public Rectangle getAttackBounds() {
		return new Rectangle(cornerx, cornery, 8, 8);

	}

	public void draw(Graphics2D p, int startx, int starty) {

		p.drawImage(rockIcon.getImage(), cornerx + startx, cornery + starty, null);

	}
}
