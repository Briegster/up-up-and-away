package Entities;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;

import Config.Config;
import GameWorld.GameWorld;

public class Spike implements ActionListener {

	int height = 1;
	int width = 1;
	int cornerx;
	int cornery;

	static boolean startDeathCount = false;
	static int deathCount;

	static ImageIcon spikeDeathIcon = new ImageIcon(Config.pathForStatic("Images/SpikeDeathAni.gif"));

	ImageIcon spikeIcon = new ImageIcon(Config.pathForStatic("Images/spike.gif"));

	public Spike(int x, int y, ImageIcon spikeIcon, int deathCount) {
		this.cornerx = x * 16;
		this.cornery = y * 16;
		this.deathCount = deathCount;

	}

	@SuppressWarnings("static-access")
	public static void deathTimer() {
		// checking if the spikes death animation is playing, and then killing
		// it.
		for (int m = 0; m < GameWorld.gameWorldFrame.arrSpike.size(); m++) {
			if (startDeathCount) {
				GameWorld.gameWorldFrame.arrSpike.get(m).deathCount++;
			}
			if (GameWorld.gameWorldFrame.arrSpike.get(m).deathCount >= 10 * GameWorld.gameWorldFrame.arrSpike.size()
					&& GameWorld.gameWorldFrame.arrSpike.get(m).spikeIcon == spikeDeathIcon) {
				GameWorld.gameWorldFrame.arrSpike.remove(m);
				startDeathCount = false;

			}
		}
	}

	public Rectangle getSpikeBounds() {
		return new Rectangle(cornerx, cornery, 16, 16);

	}

	public void draw(Graphics2D p, int startx, int starty) {
		p.drawImage(spikeIcon.getImage(), cornerx + startx, cornery + starty, null);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
}
